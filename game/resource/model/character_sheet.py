from config.action import UpperAction
import pathlib

from typing import Dict, List, NamedTuple

import pygame
import pygame.image

import typing_json

import formats.character_meta as format
from config import Direction, LowerAction, TileSize
from game.resource.model.common import get_spritesheet_rects


def load(path: pathlib.Path) -> 'CharacterSheet':
    root = path.parent
    meta_path = path.with_suffix(".meta")

    with open(meta_path, 'r') as f:
        disk_meta = typing_json.load(f, format.Main)

    images = [pygame.image.load(root / sheet_path).convert_alpha() for sheet_path in disk_meta.files]
    meta = _parse_meta(images, disk_meta)

    return CharacterSheet(images, meta)


class CharacterSheet(NamedTuple):
    surfaces: List[pygame.Surface]
    meta: 'Meta'


class Meta(NamedTuple):
    rects: List[pygame.Rect]
    frame_duration: float
    action_to_variants: Dict[LowerAction, Dict[UpperAction, List['ActionMeta']]]


class ActionMeta(NamedTuple):
    action: LowerAction
    looped: bool
    surface_index: int
    frame_count: int
    direction_to_start: Dict[Direction, int]


def _parse_meta(images: List[pygame.Surface], data: format.Main) -> Meta:
    frame_rect = pygame.Rect(0, 0, TileSize.w*data.size[0], TileSize.h*data.size[1])

    frame_rects: List[pygame.Rect] = []
    surface_starts: List[int] = []

    for image in images:
        rects = get_spritesheet_rects(image.get_rect(), frame_rect)
        surface_starts.append(len(frame_rects))
        frame_rects += rects

    action_to_variants: Dict[LowerAction, Dict[UpperAction, List[ActionMeta]]] = {}

    for lower_action in LowerAction:
        lower_action_map = data.sheets.get(lower_action.id, {})

        for upper_action in UpperAction:
            sheets = lower_action_map.get(upper_action.id, [])

            for variant in sheets:
                action = _parse_action(surface_starts, lower_action, variant)
                action_to_variants.setdefault(lower_action, {}).setdefault(upper_action, []).append(action)

    for lower_action in LowerAction:
        alias = data.lower_action_aliases.get(lower_action.id)

        if alias is not None:
            action_to_variants[lower_action] = action_to_variants[LowerAction(alias)]

    return Meta(frame_rects, data.frame_length, action_to_variants)


def _parse_action(surface_starts: List[int], action: LowerAction, data: format.Sheet) -> 'ActionMeta':
    start = surface_starts[data.file_index]
    return ActionMeta(
        action,
        action.looped or data.looped,
        data.file_index,
        data.frame_count,
        {
            direction: start + data.starts_per_direction[direction_i]
            for direction_i, direction in enumerate(Direction)
        }
    )
