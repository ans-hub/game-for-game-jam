import pathlib
import sys

# HACK: pass args to blender and move the sprite merging and selection logic out of the blender
self = pathlib.Path(__file__).parent
root = self.parent.parent

sys.path += [root.as_posix(), self.as_posix()]

from common import export  # noqa
from common import LowerActionSelector, AnimationSet, LowerAction, UpperAction, Config  # noqa


export(Config(
    layer='ghost',
    frames_per_sprite=5,
    render_resolution=(256, 256),
    sprite_size=128,
    animation_tree={
        'Ghost_Armature': LowerActionSelector({
            LowerAction.Idle: AnimationSet('Float')
        })
    },
    lower_action_aliases={
        LowerAction.Walk: LowerAction.Idle,
        LowerAction.Trot: LowerAction.Idle,
        LowerAction.Run: LowerAction.Idle,
        LowerAction.Back: LowerAction.Idle
    },
    lower_actions=(LowerAction.Idle,),
    upper_actions=(UpperAction.Rest,),
    looped_actions={
        LowerAction.Idle: True
    }
))
