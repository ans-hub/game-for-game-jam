from config.widget import HAlign, VAlign
import enum
from game.render.window import Window
from typing import List, Tuple, TypeVar, Any, Iterator, Dict
from game.ui.widgets import GenericWidget, Button
from game.core import Clock
from game.logic.input import Input
import pygame


class Row:
    def __init__(self, *widgets: GenericWidget, height: int, valign: VAlign, halign: HAlign) -> None:
        if height != 0:
            self.height = height
        else:
            max_h = 0
            for w in widgets:
                max_h = max(max_h, w.size.h)
            self.height = max_h

        self.valign = valign
        self.halign = halign
        self._widgets = widgets

    def __iter__(self) -> Iterator[GenericWidget]:
        return iter(self._widgets)


class LayoutType(enum.Enum):
    Menu = 0
    Hud = 1
    Loading = 2
    GameOver = 3
    Victory = 4
    Help = 5


TLayout = TypeVar('TLayout')


class Layout:
    def __init__(self, layout: Any) -> None:
        self.blit_list: List[Tuple[pygame.Surface, Tuple[int, int], pygame.Rect]] = []
        self.widgets_list: List[GenericWidget] = []
        self.request_activate: List[LayoutType] = []
        self.request_deactivate: List[LayoutType] = []
        self.clicked: Dict[Button, pygame.Rect] = {}

    def update(self, clock: Clock, window: Window) -> None:
        self.window_rect = window.surface.get_rect()

        for widget in self.widgets_list:
            if isinstance(widget, Button):
                widget.active_frame = 0

    def _update_click(self, input: Input) -> None:
        pos = input.mouse_position
        clicked = input.is_lmb_pressed()

        if not clicked:
            for button, rect in self.clicked.items():
                if rect.collidepoint(pos):
                    button.on_click_up()
            self.clicked.clear()
        else:
            for i, s in enumerate(self.blit_list):
                rect = pygame.Rect(s[2])
                rect.x = s[1][0]
                rect.y = s[1][1]

                widget = self.widgets_list[i]

                if isinstance(widget, Button) and clicked and rect.collidepoint(pos):
                    widget.on_click_down()
                    self.clicked[widget] = rect

    def _add_rows(self, *rows: Row, valign: VAlign, margin: int) -> None:
        rows_height = sum([r.height for r in rows])

        # assert rows_height <= self.window_rect.h

        row_y = int(self.window_rect.h * valign.vert - rows_height * valign.vert)

        for row in rows:
            widgets_width = sum([w.size.w + margin for w in row]) - margin

            curr_x = int(self.window_rect.w * row.halign.hor - widgets_width * row.halign.hor)

            for widget in row:
                surf = widget.surface
                tiles = widget.tiles
                tile_num = widget.active_frame
                rect = tiles[tile_num]

                curr_y = int(row_y + (row.height * row.valign.vert - rect.h * row.valign.vert))

                self.blit_list.append((surf, (curr_x, curr_y), rect))
                self.widgets_list.append(widget)

                curr_x += rect.w + margin

            row_y += row.height

    def _change_layout(self, deactivate: LayoutType, activate: LayoutType) -> None:
        self.request_activate.append(activate)
        self.request_deactivate.append(deactivate)
