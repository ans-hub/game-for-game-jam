import random
from typing import List
import pygame
import numpy as np

from recordclass import RecordClass

from game.components import BoidMemberComponent, EnemyTotemComponent, TransformComponent, VelocityComponent
from game.ecs import component, Storage, Scheduler, EntityHandle, Query

from game.logic import archetypes
from game.render import DebugDraw

from game.core import Clock, Color
from game.logic.system import squadron

_max_unit_speed = 200.0
_max_units_in_boid = 50

_boid_id_counter: int = 0

# ai ============================

_detect_radius = 1000.0
_update_cooldown = 20.0
_search_cooldown = 2.0

# ===============================


@component
class Component(RecordClass):
    members: List[EntityHandle] = []
    boidId: int = 0


@component
class HordeAiComponent(RecordClass):
    is_patrol: bool = True
    target_totem_index: int = 0
    target: pygame.Vector2 = pygame.Vector2(800.0, 800.0)
    time_target_updated: float = 0.0
    is_attack_state: bool = False
    time_search_updated: float = 0.0
    com_pos: pygame.Vector2 = pygame.Vector2(-1000.0, -1000.0)


# Queries ========================================================

class _HordeMember(Query):
    transform: TransformComponent
    velocity: VelocityComponent
    boid: BoidMemberComponent


class _HordeAi(Query):
    horde_ai: HordeAiComponent


class _EnemyTotemTransforms(Query):
    transform: TransformComponent
    is_enemy: EnemyTotemComponent

# =================================================================


def register_debug_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw_boids)
    scheduler.add(_draw_boid_ai)


def _draw_boids(debug_draw: DebugDraw, velocity: VelocityComponent, transform: TransformComponent) -> None:
    debug_draw.circle(Color.Red, transform.position, 10.0)
    debug_draw.line(
        Color.Blue,
        transform.position,
        transform.position + velocity.velocity * 1.0,
        2
    )


def _draw_boid_ai(debug_draw: DebugDraw, horde_ai: HordeAiComponent) -> None:
    debug_draw.circle(Color.White, horde_ai.target, 10.0)
    debug_draw.circle(Color.Red, horde_ai.target, 5.0)
    debug_draw.circle(Color.Black, horde_ai.com_pos, 8.0)


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_update_boids_movement)
    scheduler.add(_update_horde_patrol)
    scheduler.add(_update_horde_search_for_enemy)


def create_horde(storage: Storage, position: pygame.Vector2, members_count: int) -> EntityHandle:
    horde = storage.create_entity()

    global _boid_id_counter

    boids_component = Component()
    boids_component.boidId = _boid_id_counter

    ai_component = HordeAiComponent()
    if _boid_id_counter == 0:
        ai_component.is_patrol = False
    # ai_component.target = pygame.Vector2(400 + random.random() * 300, 400 + random.random() * 300)

    _boid_id_counter += 1
    storage.add_component(horde, boids_component)
    storage.add_component(horde, ai_component)

    for _ in range(members_count):
        archetypes.spawn_ghost(storage, _gen_position(position), boids_component.boidId)

    return horde


def _gen_position(position: pygame.Vector2, radius: float = 64) -> pygame.Vector2:
    radius = max(100.0, random.random() * radius)
    angle = random.random() * 360.0
    return position + pygame.Vector2(0, 1).rotate(angle) * radius


# AI systems =====================================================

def _update_horde_patrol(storage: Storage, clock: Clock, horde_ai: HordeAiComponent) -> None:
    if horde_ai.is_attack_state:
        return

    if clock.total_time - horde_ai.time_target_updated < _update_cooldown * 1000.0:
        return
    horde_ai.time_target_updated = clock.total_time

    totem_positions = []

    for transform, _ in storage.query(_EnemyTotemTransforms):
        totem_positions.append(transform.position)

    if len(totem_positions) == 0:
        return

    new_totem_to_patrol_index = 0

    if horde_ai.is_patrol:
        new_totem_to_patrol_index = random.randint(0, len(totem_positions) - 1)

    horde_ai.target_totem_index = new_totem_to_patrol_index
    horde_ai.target = _gen_position(totem_positions[new_totem_to_patrol_index])


def _update_horde_search_for_enemy(storage: Storage, clock: Clock, horde_ai: HordeAiComponent,
                                   env: squadron.Env) -> None:
    if not horde_ai.is_attack_state and clock.total_time - horde_ai.time_search_updated < _search_cooldown * 1000.0:
        return
    horde_ai.time_search_updated = clock.total_time
    horde_ai.is_attack_state = False

    _squadron = env.squadron
    assert _squadron is not None
    squadron_position = storage.get_component(_squadron, TransformComponent).position

    if (squadron_position - horde_ai.com_pos).length_squared() <= _detect_radius**2:
        horde_ai.is_attack_state = True
        horde_ai.target = squadron_position


# Boids Algorythm Params ==========================================================

cohesionFactor = 0.1
critDistSq = 50.0 * 50.0
separationFactor = 20.0
alignmentFactor = 0.05
moveToTargetFactor = 0.8

# =================================================================


def _update_boids_movement(storage: Storage, clock: Clock) -> None:
    global _boid_id_counter

    hordes_ai = []
    for horde_ai, in storage.query(_HordeAi):
        hordes_ai.append(horde_ai)

    boidsPositions = np.empty([_boid_id_counter, _max_units_in_boid, 2], dtype=np.float32)
    boidsVelocities = np.empty([_boid_id_counter, _max_units_in_boid, 2], dtype=np.float32)
    boidUnitIndexes = np.zeros(_boid_id_counter, dtype=np.uint8)

    # collect data to arrays
    for transform, velocity_comp, boid in storage.query(_HordeMember):
        pos = transform.position
        vel = velocity_comp.velocity
        boidIndex = boid.ownerBoidId
        unitIndex = boidUnitIndexes[boidIndex]
        boidsPositions[boidIndex, unitIndex] = [pos.x, pos.y]
        boidsVelocities[boidIndex, unitIndex] = [vel.x, vel.y]
        boidUnitIndexes[boidIndex] += 1

    # for every horde
    for i in range(_boid_id_counter):
        positions = boidsPositions[i]
        velocities = boidsVelocities[i]
        hordeUnitCount = boidUnitIndexes[i]
        com_pos = np.mean(positions[:hordeUnitCount], axis=0)

        # hordes_ai[i].com_pos.update(com_pos[0], com_pos[1])
        hordes_ai[i].com_pos = pygame.Vector2(com_pos[0], com_pos[1])

        sumVel = np.sum(velocities[:hordeUnitCount], axis=0)
        countVelInv = (1.0 / (boidUnitIndexes[i] - 1.0))

        v1 = np.zeros((hordeUnitCount, 2), dtype=np.float32)
        v2 = np.zeros((hordeUnitCount, 2), dtype=np.float32)
        v3 = np.zeros((hordeUnitCount, 2), dtype=np.float32)
        v4 = np.zeros((hordeUnitCount, 2), dtype=np.float32)

        # for every member in boid
        # calculate velocities
        for j in range(hordeUnitCount):
            posJ = boidsPositions[i, j]
            velJ = boidsVelocities[i, j]
            # rule 1 - cohesion
            v1[j] = (com_pos - posJ) * cohesionFactor

            # rule 2 - separation
            for k in range(hordeUnitCount):
                if k != j:
                    posK = boidsPositions[i, k]
                    deltaVec = posK - posJ
                    lengthSq = deltaVec[0] * deltaVec[0] + deltaVec[1] * deltaVec[1]
                    if lengthSq < critDistSq:
                        v2[j] = v2[j] - deltaVec * separationFactor

            # rule 3 - alignment
            avVelJ = sumVel - velJ
            avVelJ = avVelJ * countVelInv
            v3[j] = (avVelJ - velJ) * alignmentFactor

            # rule 4 - move to target
            v4[j] = (hordes_ai[i].target - posJ) * moveToTargetFactor

        # for every member in boid
        # apply velocities and positions
        for j in range(hordeUnitCount):
            posJ = boidsPositions[i, j]
            velJ = boidsVelocities[i, j]
            velJ = velJ + v1[j] + v2[j] + v3[j] + v4[j]
            speed = np.sqrt(np.sum(velJ**2))  # np.linalg.norm(velJ)
            if speed != 0.0:
                velJ = velJ / speed
            speed = min(_max_unit_speed, speed)
            velJ = velJ * speed
            posJ = posJ + velJ * clock.delta_time_s
            boidsPositions[i, j] = posJ
            boidsVelocities[i, j] = velJ

    # process data from numpy arrays back to components
    boidUnitCounter = np.zeros(_boid_id_counter, dtype=np.uint8)
    for transform, velocity_comp, boid in storage.query(_HordeMember):
        boidIndex = boid.ownerBoidId
        unitIndex = boidUnitCounter[boidIndex]
        pos = boidsPositions[boidIndex, unitIndex]
        vel = boidsVelocities[boidIndex, unitIndex]
        boidUnitCounter[boidIndex] += 1
        transform.position = pygame.Vector2(pos[0], pos[1])
        velocity_comp.velocity.x = vel[0]
        velocity_comp.velocity.y = vel[1]

        # end of horde processing
