from typing import Callable, List
import pygame
import pygame.draw

from game.render.window import Window
from game.render.camera import Camera


class PrimitiveDraw:
    def __init__(self) -> None:
        self._calls: List[Callable[[Window, Camera], pygame.Rect]] = []

    def line(self, color: pygame.Color, start: pygame.Vector2, end: pygame.Vector2, width: int) -> None:
        self._calls.append(
            lambda w, c: pygame.draw.line(w.surface, color, start - c.top_left, end - c.top_left, width)
        )

    def draw(self, window: Window, camera: Camera) -> None:
        for call in self._calls:
            call(window, camera)

        self.clear()

    def clear(self) -> None:
        self._calls.clear()
