from game.ecs.scheduler import Scheduler
from game.ui.layouts.loading import LoadingLayout

import pygame

from game.resource import Loader

from game.core import Clock
from game.ecs import Storage

from game.render.debug_draw import DebugDraw
from game.render.primitive_draw import PrimitiveDraw
from game.render.system import character_animation, sprite_draw, camera_controller, culling
from game.render import Window, Camera, Terrain

from game.logic import archetypes
from game.logic.system import boids, music, navigation, sound, squadron, totem, shooting
from game.ui import Ui
from game.logic.input import Input
from game.sound.mixer import Mixer

from game.ui.system import game_over
from game.ui.layouts.hud import HudLayout
from game.ui.layouts.menu import MenuLayout
from game.ui.layouts.gameover import GameOverLayout
from game.ui.layouts.victory import VictoryLayout
from game.ui.layouts.help import HelpLayout


class World:
    def __init__(self, loader: Loader, screen_size: pygame.Rect) -> None:
        self.game_started = False

        self.storage = Storage()
        self.debug_draw = DebugDraw()
        self.primitive_draw = PrimitiveDraw()
        self.update_scheduler = Scheduler(Input, Clock, Camera, Mixer, Ui)
        self.draw_scheduler = Scheduler(Clock, Window, Camera, PrimitiveDraw)
        self.debug_draw_scheduler = Scheduler(Window, Camera, DebugDraw)
        self.terrain = Terrain(loader, screen_size)
        self.ui = Ui()

        archetypes.init(self.storage, loader)

        squadron.register_update(self.storage, self.update_scheduler)
        boids.register_update(self.update_scheduler)
        totem.register_update(self.update_scheduler)
        camera_controller.register_update(self.update_scheduler)
        culling.register_update(self.update_scheduler)
        character_animation.register_update(self.update_scheduler)
        sound.register_update(self.update_scheduler)
        shooting.register_update(self.update_scheduler)
        music.register_update(self.storage, self.update_scheduler, loader)

        sprite_draw.register_draw(self.draw_scheduler)
        shooting.register_draw(self.draw_scheduler)
        shooting.register_debug_draw(self.debug_draw_scheduler)
        totem.register_draw(self.draw_scheduler)

        navigation.register_draw(self.storage, self.draw_scheduler, loader)

        totem.register_debug_draw(self.debug_draw_scheduler)
        boids.register_debug_draw(self.debug_draw_scheduler)
        squadron.register_debug_draw(self.debug_draw_scheduler)

        self.ui.register_layout(LoadingLayout(loader))
        self.ui.register_layout(HudLayout(loader, self.storage))
        self.ui.register_layout(MenuLayout(loader, lambda: self.start()))
        self.ui.register_layout(GameOverLayout(loader))
        self.ui.register_layout(VictoryLayout(loader))
        self.ui.register_layout(HelpLayout(loader))

    def start(self) -> None:
        if self.game_started:
            return

        self.game_started = True

        game_over.register_update(self.update_scheduler)

        for object in self.terrain.objects:
            t = object.properties['type']

            if t == 'spawner':
                is_enemy = object.properties['enemy']
                souls = object.properties['souls']
                health = object.properties.get('health', 10)
                cooldown = object.properties.get('cooldown', 3.0)

                if not is_enemy:
                    squadron.create(self.storage, pygame.Vector2(object.position.x, object.position.y + 200.0))

                assert isinstance(is_enemy, bool)
                assert isinstance(souls, int)
                assert isinstance(health, int)
                assert isinstance(cooldown, float)

                archetypes.spawn_totem(self.storage, object.position, is_enemy, souls, cooldown, health)
            else:
                assert False, "Unknown object type {}".format(t)

    def update(self, input: Input, clock: Clock, camera: Camera, window: Window, mixer: Mixer) -> None:
        self.storage.update()

        self.update_scheduler.run(self.storage, (input, clock, camera, mixer, self.ui))

        self.ui.update(clock, window, input)
        self.terrain.update(camera)

    def draw(self, clock: Clock, window: Window, camera: Camera, draw_debug: bool) -> None:
        self.terrain.draw(window, camera)

        self.draw_scheduler.run(self.storage, (clock, window, camera, self.primitive_draw))
        self.primitive_draw.draw(window, camera)
        self.ui.draw(window)
        window.draw_text("{}".format(self.storage.entity_count), 0, 20)

        if draw_debug:
            self.debug_draw_scheduler.run(self.storage, (window, camera, self.debug_draw))
            self.debug_draw.draw(window, camera)
        else:
            self.debug_draw.clear()
