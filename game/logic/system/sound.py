import random

import pygame.mixer

from typing import Optional

from recordclass import RecordClass

from config import LowerAction

from game.ecs import Scheduler, component

from game.core import Clock
from game.sound import Mixer
from game.resource.model import Sound

from game.render.system import character_animation


@component
class Component(RecordClass):
    sound: Sound

    action: Optional[LowerAction] = None
    channel: Optional[pygame.mixer.Channel] = None
    length_left: float = 0.0


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_switch_by_action)
    scheduler.add(_update_duration)


def _switch_by_action(mixer: Mixer, component: Component, animation: character_animation.Component) -> None:
    if animation.lower_action != component.action:
        if component.action is not None:
            _stop(mixer, component)

        if animation.lower_action != LowerAction.Idle:
            _play(mixer, component)

        component.action = animation.lower_action


def _update_duration(clock: Clock, mixer: Mixer, component: Component) -> None:
    if component.channel is None:
        return

    component.length_left -= clock.delta_time_s

    if component.length_left < 0:
        _stop(mixer, component)

        if component.sound.meta.repeat:
            _play(mixer, component)


def _play(mixer: Mixer, component: Component) -> None:
    sound = random.choice(component.sound.sounds)
    component.channel = mixer.play(sound)
    if component.channel is not None:
        component.length_left = sound.get_length()


def _stop(mixer: Mixer, component: Component) -> None:
    if component.channel is not None:
        mixer.stop(component.channel)
        component.length_left = 0
        component.channel = None
