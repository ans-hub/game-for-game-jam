from game.render.window import Window
from typing import Any, Dict, Set
from game.logic.input import Input
from game.ui.layout import Layout, LayoutType, Button
from game.core import Clock


class Ui:
    def __init__(self) -> None:
        self.active_layouts: Dict[LayoutType, bool] = {}
        self.layouts: Dict[LayoutType, Layout] = {}

    def register_layout(self, layout: Any) -> None:
        assert layout.type not in self.layouts.keys()
        self.layouts[layout.type] = layout

    def activate_layout(self, layout_type: LayoutType) -> None:
        self.active_layouts[layout_type] = True

    def deactivate_layout(self, layout_type: LayoutType) -> None:
        self.active_layouts[layout_type] = False

    def _update_click(self, input: Input) -> None:
        for type, active in self.active_layouts.items():
            if not active:
                continue

            self.layouts[type]._update_click(input)

    def update(self, clock: Clock, window: Window, input: Input) -> None:
        to_activate: Set[LayoutType] = set()
        to_deactivate: Set[LayoutType] = set()

        for type, active in self.active_layouts.items():
            if not active:
                continue

            layout = self.layouts[type]

            layout.update(clock, window)

            for widget in layout.widgets_list:
                if isinstance(widget, Button):
                    widget.active_frame = 0

        self._update_click(input)

        for type, active in self.active_layouts.items():
            if not active:
                continue

            for req in layout.request_activate:
                to_activate.add(req)

            for req in layout.request_deactivate:
                to_deactivate.add(req)

            layout.request_activate.clear()
            layout.request_deactivate.clear()

        for layout_type in to_activate:
            self.activate_layout(layout_type)

        for layout_type in to_deactivate:
            self.deactivate_layout(layout_type)

    def draw(self, window: Window) -> None:
        for type, active in self.active_layouts.items():
            if not active:
                continue

            layout = self.layouts[type]

            window.surface.blits(layout.blit_list, doreturn=False)  # type: ignore

            layout.blit_list = []
            layout.widgets_list = []
