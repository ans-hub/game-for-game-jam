import sys
import typing


if sys.version_info.minor >= 9:
    if not typing.TYPE_CHECKING:
        from typing import _NamedTuple, NamedTupleMeta  # type:ignore

        # HACK: fields required by typing_jsons are no longer provided by python 3.9
        class DiskNamedTupleMeta(NamedTupleMeta):
            def __new__(cls, typename, bases, ns):
                class_ = super().__new__(cls, typename, (_NamedTuple,), ns)

                if sys.version_info >= (3, 9):
                    class_._field_types = class_.__annotations__

                return class_

        DiskNamedTuple = type.__new__(DiskNamedTupleMeta, 'DiskNamedTuple', (), {})
else:
    DiskNamedTuple = typing.NamedTuple


class DiskRange(DiskNamedTuple):
    start: int
    end: int
