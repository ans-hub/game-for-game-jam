from game.resource.model.sound import Sound
from typing import NamedTuple
from game.ui.layout import Row
from game.ui.widgets import Caption, LoadingBar
from game.resource import Request, Collection
from game.ui import ui
from game.render import Window
from game.core import Clock
from game.resource.model.ui_widget import UiWidget
from game.resource.loader import Loader
from game.ui.widgets import Image, Button
from game.ui.layout import Row
from game.resource.loader import _load
from config.widget import Anchor, VAlign, HAlign
from game.ecs import component
from recordclass.typing import RecordClass


class _Assets(Collection):
    logo: UiWidget = Request("logo")
    img_gameover: UiWidget = Request("img_gameover")


_data: _Assets


class GameOverLayout(ui.Layout):
    def __init__(self, loader: Loader) -> None:
        super().__init__(loader)

        global _data
        _data = loader.load(_Assets)

        self.type = ui.LayoutType.GameOver
        self.img_logo: Image = Image("logo", assets=_data)
        self.img_gameover: Image = Image("img_gameover", assets=_data)

    def update(self, clock: Clock, window: Window) -> None:
        global _data

        super().update(clock, window)

        self._add_rows(
            Row(
                self.img_gameover,
                height=self.window_rect.h,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=20
        )
        self._add_rows(
            Row(
                self.img_logo,
                height=window.scr_h // 2,
                valign=VAlign.Bottom, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=40
        )
