from typing import Any, Callable, Generator, Iterator, List, TYPE_CHECKING, Tuple, Type, TypeVar, cast, Set, Dict

import numpy as np

from game.core.utils import swap_elements

from game.ecs.entity_handle import EntityHandle


T = TypeVar('T')

component_type_count = 0
env_type_count = 0

_query_id_to_types: Dict[np.uint32, Tuple[type, ...]] = {}


if not TYPE_CHECKING:
    Query = None


class _QueryMeta(type):
    def __new__(cls, typename, bases, ns):
        annotations = ns.get('__annotations__', {})
        types = tuple([t for name, t in annotations.items() if not name.startswith("_")])
        query = super().__new__(cls, typename, bases, {})

        if len(types) > 0 and Query is not None:
            name = "{}_{}".format(cls.__module__.replace('.', '_'), typename)

            fetch_code_parts = []
            component_types: List[type] = []

            for t in types:
                if t is EntityHandle:
                    fetch_code_parts.append(make_handle_fetch_code())
                else:
                    fetch_code_parts.append(make_component_fetch_code(t))
                    component_types.append(t)

            iterator_code = make_query_for_code(component_types)
            fetch_code = "(({},) {})".format(",".join(fetch_code_parts), iterator_code)
            fetch = compile(fetch_code, name, 'eval')

            def call_fetch(storage: Storage) -> Generator[Query, None, None]:
                return eval(fetch, {"storage": storage, "EntityHandle": EntityHandle})

            query._fetch = call_fetch

        return query


class Query(metaclass=_QueryMeta):
    _fetch: Callable[['Storage'], Generator['Query', None, None]]

    def __iter__(self) -> Iterator[Any]:
        return iter(self)


TQuery = TypeVar('TQuery', bound=Query)


def component(t: Type[T]) -> Type[T]:
    global component_type_count

    assert not is_component_type(t), "Type already registered as ECS component"
    assert not is_env_type(t), "Type already registered as ECS env"

    i = component_type_count
    component_type_count += 1

    assert component_type_count <= 32, "Too many component types"

    setattr(t, "_ecs_component_type_id", i)
    setattr(t, "_ecs_component_type_mask", _make_type_mask(i))

    t.__name__ = "{}.{}".format(t.__module__, t.__name__)

    return t


def env(t: Type[T]) -> Type[T]:
    global env_type_count

    assert not is_component_type(t), "Type is already registered as ECS component"
    assert not is_env_type(t), "Type is already registered as ECS env"

    i = env_type_count
    env_type_count += 1

    setattr(t, "_ecs_env_type_id", i)

    t.__name__ = "{}.{}".format(t.__module__, t.__name__)

    return t


class Storage:
    def __init__(self) -> None:
        self.entity_count = 0

        self._component_arrays = tuple(
            _ComponentArray(_make_type_mask(i)) for i in range(component_type_count)
        )

        self._env: list = [None] * component_type_count

        self._entity_to_index = np.zeros(_entity_page_size, dtype=np.uint32)

        self._layouts = np.zeros(_entity_page_size, dtype=np.uint32)
        self._groups = _Groups()
        self._handles = np.zeros(_entity_page_size, dtype=np.uint32)

        self._free_indices: List[int] = []
        self._serial = np.uint8(1)

        self._last_entity_id = 0

        self._entity_count_capacity = _entity_page_size

        self._destroy_queue: Set[EntityHandle] = set()
        self._create_queue: Set[EntityHandle] = set()
        self._add_component_queue: List[Tuple[EntityHandle, Any]] = []
        self._remove_component_queue: List[Tuple[EntityHandle, type]] = []

    @property
    def entities(self) -> Generator[EntityHandle, None, None]:
        return (handle for handle in self._handles if handle != EntityHandle.InvalidValue)

    def add_env(self, env: T) -> None:
        t = type(env)
        assert is_env_type(t), "Type {} is not a ECS env"
        type_id = _get_env_type_id(t)
        assert self._env[type_id] is None, "Env {} already added to this Storage"
        self._env[type_id] = env

    def get_env(self, t: Type[T]) -> T:
        assert is_env_type(t), "Type {} is not a ECS env"
        type_id = _get_env_type_id(t)
        env = self._env[type_id]
        assert env is not None, "Env {} was not added to this Storage"
        return env

    def create_entity(self) -> EntityHandle:
        if len(self._free_indices) > 0:
            index = self._free_indices.pop()
        else:
            index = self._last_entity_id
            self._last_entity_id += 1

        handle = EntityHandle.make(index, int(self._serial))

        self._serial = np.add(self._serial, np.uint8(2))

        self._create_queue.add(handle)

        return handle

    def destroy_entity(self, entity: EntityHandle) -> None:
        self._destroy_queue.add(entity)

    def has_component(self, entity: EntityHandle, t: Type[T]):
        assert entity not in self._create_queue, "Can't check components for an entity just created"
        self._validate(entity)
        index = self._entity_to_index[entity.index]

        return self._layouts[index] & _get_component_type_mask(t) != 0

    def get_component(self, entity: EntityHandle, t: Type[T]) -> T:
        assert entity not in self._create_queue, "Can't fetch components for an entity just created"
        self._validate(entity)
        type_id = _get_component_type_id(t)
        index = self._entity_to_index[entity.index]

        assert self._layouts[index] & _get_component_type_mask(t) != 0, \
            "Entity does not have the component {}".format(t)

        return cast(T, self._component_arrays[type_id].get(index))

    def add_component(self, entity: EntityHandle, component: T) -> None:
        self._validate(entity)

        self._add_component_queue.append((entity, component))

    def remove_component(self, entity: EntityHandle, t: Type[T]) -> None:
        self._validate(entity)
        self._remove_component_queue.append((entity, t))

    def query(self, query: Type[TQuery]) -> Generator[TQuery, None, None]:
        return cast(Generator[TQuery, None, None], query._fetch(self))

    def update(self) -> None:
        if self._last_entity_id > self._entity_count_capacity:
            self._resize_arrays()

        self._destroy_entities()
        self._create_entities()
        self._remove_components()
        self._add_components()

        self._destroy_queue.clear()
        self._create_queue.clear()
        self._remove_component_queue.clear()
        self._add_component_queue.clear()

    def _resize_arrays(self) -> None:
        self._entity_count_capacity = ((self._last_entity_id // _entity_page_size) + 2) * _entity_page_size
        self._layouts.resize(self._entity_count_capacity, refcheck=False)
        self._handles.resize(self._entity_count_capacity, refcheck=False)
        self._entity_to_index.resize(self._entity_count_capacity, refcheck=False)

        for array in self._component_arrays:
            array.resize(self._entity_count_capacity)

    def _destroy_entities(self) -> None:
        remaining_entity_count = self.entity_count - len(self._destroy_queue)
        last_index = self.entity_count - 1

        for entity in self._destroy_queue:
            index = self._entity_to_index[entity.index]

            if last_index > index:
                last_handle = EntityHandle(self._handles[last_index])

                self._entity_to_index[last_handle.index] = index

                self._groups.remove(index, self._layouts[index])
                self._groups.remove(np.uint32(last_index), self._layouts[last_index])

                swap_elements(self._handles, index, last_index)
                swap_elements(self._layouts, index, last_index)

                self._groups.migrate(index, np.uint32(0), self._layouts[index])

                for array in self._component_arrays:
                    array.move(index, last_index)
            else:
                self._groups.remove(index, self._layouts[index])

                self._handles[index] = 0
                self._layouts[index] = 0
                for array in self._component_arrays:
                    array.remove(index)

            self._entity_to_index[entity.index] = 0

            last_index -= 1

            self._free_indices.append(entity.index)

        self._layouts[remaining_entity_count:self.entity_count] = 0
        self._handles[remaining_entity_count:self.entity_count] = 0

        self.entity_count = remaining_entity_count

    def _create_entities(self) -> None:
        for entity in self._create_queue:
            index = self.entity_count
            self._entity_to_index[entity.index] = index
            self._handles[index] = entity.value
            self._layouts[index] = 0
            self.entity_count += 1

    def _remove_components(self) -> None:
        for entity, t in self._remove_component_queue:
            if entity in self._destroy_queue:
                continue

            index = self._entity_to_index[entity.index]
            type_id = _get_component_type_id(t)
            self._component_arrays[type_id].remove(index)
            old_layout = self._layouts[index]
            new_layout = old_layout & ~_get_component_type_mask(t)
            self._layouts[index] = new_layout
            self._groups.migrate(index, old_layout, new_layout)

    def _add_components(self) -> None:
        for entity, component in self._add_component_queue:
            assert entity not in self._destroy_queue

            t = type(component)
            type_id = _get_component_type_id(t)
            index = self._entity_to_index[entity.index]
            self._component_arrays[type_id].set(index, component)
            old_layout = self._layouts[index]
            new_layout = old_layout | _get_component_type_mask(t)
            self._layouts[index] |= new_layout
            self._groups.migrate(index, old_layout, new_layout)

    def _unsafe_query_entities(self, query_id: np.uint32) -> Generator[int, None, None]:
        return (
            int(entity)
            for layout, entities in self._groups.raw.items()
            if layout & query_id == query_id
            for entity in entities
        )

    def _validate(self, entity: EntityHandle) -> None:
        assert entity in self._create_queue or self._handles[self._entity_to_index[entity.index]] == entity.value, \
            "Accessing deleted Entity"


def is_component_type(t: type) -> bool:
    return hasattr(t, "_ecs_component_type_id")


def is_env_type(t: type) -> bool:
    return hasattr(t, "_ecs_env_type_id")


def _get_env_type_id(t: type) -> int:
    return cast(_EnvStub, t)._ecs_env_type_id


def _get_component_type_id(t: type) -> int:
    return cast(_ComponentStub, t)._ecs_component_type_id


def _get_component_type_mask(t: type) -> np.uint32:
    return cast(_ComponentStub, t)._ecs_component_type_mask


def make_query_for_code(types: Tuple[type, ...]) -> str:
    id_ = _make_query_id(types)
    return "for i in storage._unsafe_query_entities({})".format(id_)


def make_handle_fetch_code() -> str:
    return "EntityHandle(storage._handles[i])"


def make_component_fetch_code(t: type) -> str:
    type_id = _get_component_type_id(t)
    return "storage._component_arrays[{}].get(i)".format(type_id)


def make_env_fetch_code(t: type) -> str:
    type_id = _get_env_type_id(t)
    return "storage._env[{}]".format(type_id)


_entity_page_size = 128


class _ComponentStub:
    _ecs_component_type_id: int
    _ecs_component_type_mask: np.uint32


class _EnvStub:
    _ecs_env_type_id: int


class _Groups:
    def __init__(self) -> None:
        self.raw: Dict[np.uint32, Set[np.uint32]] = {}

    def _ensure_layout(self, layout: np.uint32) -> Set[np.uint32]:
        return self.raw.setdefault(layout, set())

    def migrate(self, index: np.uint32, old_layout: np.uint32, new_layout: np.uint32) -> None:
        self._ensure_layout(old_layout).discard(index)
        self._ensure_layout(new_layout).add(index)

    def remove(self, index: np.uint32, layout: np.uint32) -> None:
        self._ensure_layout(layout).discard(index)


class _ComponentArray:
    def __init__(self, mask: np.uint32):
        self.mask = mask

        self._components: List[object] = [None] * _entity_page_size

    def resize(self, capacity: int) -> None:
        if capacity > len(self._components):
            self._components += [None] * (capacity - len(self._components))

    def get(self, index: int) -> object:
        return self._components[index]

    def set(self, index: int, component: T) -> None:
        assert self._components[index] is None, "Entity already has component {}".format(type(component))
        self._components[index] = component

    def remove(self, index: int) -> None:
        self._components[index] = None

    def move(self, dst: int, src: int) -> None:
        self._components[dst] = self._components[src]
        self._components[src] = None


def _make_query_id(types: Tuple[type, ...]) -> np.uint32:
    id_ = np.uint32(0)

    for type_ in types:
        assert is_component_type(type_), "Type {} is not a component".format(type_)
        id_ = np.bitwise_or(id_, _get_component_type_mask(type_))

    _query_id_to_types[id_] = types

    return id_


def _make_type_mask(i: int) -> np.uint32:
    return np.left_shift(1, np.uint32(i))
