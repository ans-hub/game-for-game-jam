import pygame

from game.ecs import Storage, Scheduler, EntityHandle

from game.core import Clock, Color
from game.components import EnemyTotemComponent, TotemComponent, TransformComponent
from game.render import DebugDraw, PrimitiveDraw
from game.logic.system import boids, squadron
from game.logic import archetypes

_spawn_limit = 12
_spawn_count = 0
_activate_radius = 100

_distance_to_healthbar = 120.0
_healthbar_width = 250.0
_healthbar_thinkness = 5


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_spawn_units_by_time)
    scheduler.add(_spawn_allies_units)
    scheduler.add(_activate_totem)


def register_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw_health)


def register_debug_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw_position)


def _draw_position(debug_draw: DebugDraw, totem: TotemComponent, transform: TransformComponent) -> None:
    debug_draw.circle(Color.Black if totem.is_enemy else Color.Green, transform.position, 30.0)
    debug_draw.text("{}".format(totem.souls), transform.position)


def _draw_health(
    primitive_draw: PrimitiveDraw,
    totem: TotemComponent,
    transform: TransformComponent,
    _: EnemyTotemComponent
) -> None:
    healthbar_center = pygame.Vector2(transform.position.x, transform.position.y - _distance_to_healthbar)

    healthbar_start = pygame.Vector2(healthbar_center.x - _healthbar_width/2, healthbar_center.y)
    healthbar_end = pygame.Vector2(healthbar_center.x + _healthbar_width/2, healthbar_center.y)

    if totem.health == totem.original_health:
        primitive_draw.line(Color.Green, healthbar_start, healthbar_end, _healthbar_thinkness)
    else:
        split = totem.health / totem.original_health
        healthbar_split = pygame.Vector2(healthbar_start.x + _healthbar_width*split, healthbar_start.y)

        primitive_draw.line(Color.Green, healthbar_start, healthbar_split, _healthbar_thinkness)
        primitive_draw.line(Color.Red, healthbar_split, healthbar_end, _healthbar_thinkness)


# Enemy totem ==============================================================

def _spawn_units(storage: Storage, _totem: EntityHandle) -> None:
    global _spawn_count
    if _spawn_count >= _spawn_limit:
        return

    totemComp = storage.get_component(_totem, TotemComponent)
    center = storage.get_component(_totem, TransformComponent).position
    boids.create_horde(storage, center, totemComp.souls)
    _spawn_count += 1


def _spawn_units_by_time(storage: Storage, clock: Clock, _totem: EntityHandle, totem_comp: TotemComponent) -> None:
    if not totem_comp.is_enemy:
        return
    if clock.total_time - totem_comp.spawn_last_time >= totem_comp.spawn_cooldown * 1000.0:
        totem_comp.spawn_last_time = clock.total_time
        _spawn_units(storage, _totem)


# Player's totem ============================================================


def _spawn_allies_units(storage: Storage, clock: Clock, totem_comp: TotemComponent) -> None:
    if totem_comp.is_enemy or totem_comp.souls <= 0:
        return

    if clock.total_time - totem_comp.spawn_last_time >= totem_comp.spawn_cooldown * 1000.0:
        totem_comp.spawn_last_time = clock.total_time
        totem_comp.souls -= 1
        archetypes.spawn_rider(storage)


def _activate_totem(storage: Storage, env: squadron.Env, totem_comp: TotemComponent, _totem: EntityHandle) -> None:
    if totem_comp.is_enemy:
        return

    _squadron = env.squadron
    assert _squadron is not None

    totem_pos = storage.get_component(_totem, TransformComponent).position
    squadron_component = storage.get_component(_squadron, squadron.Component)
    squadron_position = storage.get_component(_squadron, TransformComponent).position
    if (squadron_position - totem_pos).length_squared() <= _activate_radius**2:
        totem_comp.souls += squadron_component.souls
        squadron_component.souls = 0


# ============================================================================
