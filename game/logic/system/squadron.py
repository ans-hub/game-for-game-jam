import random
from typing import List, NamedTuple, Optional

import pygame
import math
import numpy as np

from recordclass import RecordClass

from game.components import BoidMemberComponent, DeadComponent, SpawnedComponent, TransformComponent, VelocityComponent
from game.components import CameraAnchorComponent
from game.ecs import component, env, Storage, Scheduler, EntityHandle, Query
from game.core import Color, Clock
from game.core.utils import normalize_angle

from game.render import DebugDraw

from game.logic.input import Input


# HACK: because python is slow and we don't multi-threading anyway, we are sharing this between riders and squadron
class _Formation(RecordClass):
    id_to_cell: np.ndarray = np.zeros((0, 0), dtype=np.int8)  # rider index -> row, column

    # row, column -> (position(xy), velocity (v), rotation - also vector, but y coordinate ignored)
    cell_to_xy_v_r: np.ndarray = np.zeros((0, 0, 3, 2), dtype=np.float32)

    # We are cheating here - only first row is actually pathfinded
    # followers just run towards the rider in front of them, ignoring speed/rotation restrictions


@component
class Component(RecordClass):
    left_speed: float = 0
    right_speed: float = 0

    breadth: int = 1
    rider_count: int = 0
    row_count: int = 0
    column_count: int = 0

    free_rider_ids: List[int] = []
    allocated_rider_ids: List[int] = []
    max_id: int = 0

    souls: int = 0

    formation: _Formation = _Formation()


@component
class RiderComponent(RecordClass):
    id: int = -1
    formation: _Formation = _Formation()
    shift: pygame.Vector2 = pygame.Vector2(0, 0)


@env
class Env(RecordClass):
    squadron: Optional[EntityHandle] = None


def register_update(storage: Storage, scheduler: Scheduler) -> None:
    scheduler.add(_update_order)
    scheduler.add(_move_squadron)
    scheduler.add(_sync_riders)
    scheduler.add(_detect_enemy_collisions)

    storage.add_env(Env())


def register_debug_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw_pathfinding)


def create(storage: Storage, position: pygame.Vector2):
    env = storage.get_env(Env)

    assert env.squadron is None, \
        "Only one squadron is supported. Create a unique tag component to filter squadron members, if needed"

    squadron = storage.create_entity()

    storage.add_component(squadron, VelocityComponent())
    storage.add_component(squadron, CameraAnchorComponent())
    storage.add_component(squadron, TransformComponent(position))
    storage.add_component(squadron, Component())

    env.squadron = squadron


def remove_rider(storage: Storage, squadron_handle: EntityHandle) -> None:
    squadron = storage.get_component(squadron_handle, Component)

    if len(squadron.riders) == 0:
        return

    if len(squadron.riders) > 1:
        index = random.randint(1, len(squadron.riders) - 1)
    else:
        index = 0

    rider = squadron.riders[index]

    storage.add_component(rider, DeadComponent())


def _draw_pathfinding(debug_draw: DebugDraw, squadron: Component, transform: TransformComponent) -> None:
    pf = squadron.formation

    debug_draw.circle(Color.Black, transform.position, 15.0)

    direction = _make_direction(transform.rotation)
    orhogonal = direction.rotate(90)

    width = _formation_cell_size*squadron.breadth
    left = transform.position - orhogonal*width//2
    right = transform.position + orhogonal*width//2

    debug_draw.circle(Color.Red, left, 15)
    debug_draw.line(Color.Red, left, left + direction*squadron.left_speed, 5)
    debug_draw.circle(Color.Green, right, 15)
    debug_draw.line(Color.Green, right, right + direction*squadron.right_speed, 5)

    for i in range(squadron.row_count):
        for j in range(squadron.column_count):
            xy_v_r = pf.cell_to_xy_v_r[i, j]
            position = pygame.Vector2(*xy_v_r[0])
            velocity = pygame.Vector2(*xy_v_r[1])
            debug_draw.circle(Color.Blue, position, 15.0)
            debug_draw.line(Color.Blue, position, position + velocity, 5)


class _SpawnedRider(Query):
    handle: EntityHandle
    rider: RiderComponent

    spawned: SpawnedComponent


class _DeadMember(Query):
    handle: EntityHandle
    rider: RiderComponent
    dead: DeadComponent


def _free_id(squadron: Component, id: int) -> None:
    squadron.allocated_rider_ids.remove(id)
    squadron.free_rider_ids.append(id)


def _allocate_id(squadron: Component) -> int:
    if len(squadron.free_rider_ids) > 0:
        id_ = squadron.free_rider_ids.pop()
    else:
        id_ = squadron.max_id
        squadron.max_id += 1

    squadron.allocated_rider_ids.append(id_)

    return id_


def _update_order(
    storage: Storage,
    squadron: Component,
    transform: TransformComponent
) -> None:
    dirty = False

    for handle, rider, _ in storage.query(_DeadMember):
        _free_id(squadron, rider.id)

        squadron.rider_count -= 1

        storage.destroy_entity(handle)
        dirty = True

    # These actions happening at the same time are unlikely
    for handle, rider, _ in storage.query(_SpawnedRider):
        rider.id = _allocate_id(squadron)
        rider.formation = squadron.formation
        rider.shift = pygame.Vector2(random.uniform(*_random_shift), random.uniform(*_random_shift))

        squadron.rider_count += 1

        storage.remove_component(handle, SpawnedComponent)
        dirty = True

    if not dirty or squadron.rider_count == 0:
        return

    formation = squadron.formation

    formation.id_to_cell.resize((squadron.max_id, 2), refcheck=False)

    squadron.breadth = int(math.ceil(squadron.rider_count / _max_depth))

    if squadron.breadth % 2 == 0:
        squadron.breadth += 1

    squadron.row_count = int(math.ceil(squadron.rider_count / squadron.breadth))
    squadron.column_count = squadron.breadth

    formation.cell_to_xy_v_r.resize((squadron.row_count, squadron.column_count, 3, 2), refcheck=False)

    for i, id in enumerate(squadron.allocated_rider_ids):
        column = i % squadron.breadth
        row = i // squadron.breadth

        formation.id_to_cell[id] = (row, column)

    for row in range(squadron.row_count):
        _set_row_position_and_velocity(row, squadron, transform)


def _set_row_position_and_velocity(row: int, squadron: Component, transform: TransformComponent) -> None:
    formation = squadron.formation

    direction = _make_direction(transform.rotation)
    orthogonal = direction.rotate(90)

    width = squadron.breadth*_formation_cell_size

    angular_speed = (squadron.left_speed - squadron.left_speed) / width

    from_center_to_left = (squadron.breadth // 2)*_formation_cell_size

    for column in range(squadron.column_count):
        distance_from_left = column*_formation_cell_size
        from_center = distance_from_left - from_center_to_left

        depth = direction*_formation_cell_size*row
        position = transform.position - depth + from_center*orthogonal

        xy_v_r = formation.cell_to_xy_v_r[row, column]
        cell_speed = squadron.left_speed + angular_speed*column*_formation_cell_size
        velocity = cell_speed * direction

        xy_v_r[0] = (position.x, position.y)
        xy_v_r[1] = (velocity.x, velocity.y)
        xy_v_r[2] = (transform.rotation, 0)


def _move_squadron(
    clock: Clock,
    input: Input,
    squadron: Component,
    velocity: VelocityComponent,
    transform: TransformComponent
) -> None:
    goal = _get_goal(input, velocity.velocity, transform.rotation)

    if goal.want_rotation != transform.rotation or goal.want_speed != goal.current_speed:
        steering = _steer_squadron(clock, squadron, transform, goal)

        squadron.left_speed = steering.left_speed
        squadron.right_speed = steering.right_speed

        transform.rotation = normalize_angle(transform.rotation + steering.rotation_change)

        velocity.velocity = _make_direction(transform.rotation)*steering.middle_speed

    transform.position += velocity.velocity*clock.delta_time_s

    _update_formation(clock, squadron, transform)


def _sync_riders(
    storage: Storage,
    entity: EntityHandle,
    rider: RiderComponent,
    transform: TransformComponent,
    velocity: VelocityComponent
) -> None:
    if storage.has_component(entity, DeadComponent):
        return

    formation = rider.formation
    row, column = formation.id_to_cell[rider.id]
    xy_v_r = formation.cell_to_xy_v_r[row, column]

    transform.position = pygame.Vector2(*xy_v_r[0]) + rider.shift
    velocity.velocity = pygame.Vector2(*xy_v_r[1])
    transform.rotation = xy_v_r[2][0]


class _GoalInfo(NamedTuple):
    current_speed: float
    want_speed: float
    want_rotation: float


class _Steering:
    def __init__(
        self,
        breadth: float,
        to_rotate: float,
        left_speed: float,
        right_speed: float,
        dt: float,
        goal: _GoalInfo
    ) -> None:
        self.width = breadth * _formation_cell_size

        self._speeds = [left_speed, right_speed]

        if to_rotate < 0:
            self._inner_index = 1
            self._outer_index = 0
            self.sign = -1
        else:
            self._outer_index = 1
            self._inner_index = 0
            self.sign = +1

        self.time_left = dt
        self.goal = goal

        self.rotation_required = to_rotate
        self.rotation_change = 0.0

        self._min_index: int
        self._max_index: int

        self.check_wrong_direction()
        self.update(0)

    def update(self, dt: float):
        self.angular_speed = self.sign * (self.outer_speed - self.inner_speed) / self.width
        self.rotation_change += math.degrees(self.angular_speed*dt)
        self.time_left -= dt

    def check_wrong_direction(self) -> bool:
        if self.inner_speed > self.outer_speed:
            self._min_index = self._outer_index
            self._max_index = self._inner_index
            self.wrong_direction = True
        else:
            self._max_index = self._outer_index
            self._min_index = self._inner_index
            self.wrong_direction = False

        return self.wrong_direction

    @property
    def rotation_left(self) -> float:
        return self.rotation_required - self.rotation_change

    @property
    def middle_speed(self) -> float:
        return (self.max_speed + self.min_speed) / 2

    @property
    def min_speed(self) -> float:
        return self._speeds[self._min_index]

    @min_speed.setter
    def min_speed(self, value: float) -> None:
        self._speeds[self._min_index] = value

    @property
    def max_speed(self) -> float:
        return self._speeds[self._max_index]

    @max_speed.setter
    def max_speed(self, value: float) -> None:
        self._speeds[self._max_index] = value

    @property
    def inner_speed(self) -> float:
        return self._speeds[self._inner_index]

    @inner_speed.setter
    def inner_speed(self, value: float) -> None:
        self._speeds[self._inner_index] = value

    @property
    def outer_speed(self) -> float:
        return self._speeds[self._outer_index]

    @outer_speed.setter
    def outer_speed(self, value: float) -> None:
        self._speeds[self._outer_index] = value

    @property
    def left_speed(self) -> float:
        return self._speeds[0]

    @property
    def right_speed(self) -> float:
        return self._speeds[1]


def _get_time_to_equalize(context: _Steering) -> float:
    max_speed_diff = context.max_speed - context.middle_speed
    min_speed_diff = context.middle_speed - context.min_speed

    return max(max_speed_diff/_deceleration, min_speed_diff/_acceleration)


def _get_time_to_accelerate_inner_to_outer(context: _Steering) -> float:
    return (context.outer_speed - context.inner_speed)/_acceleration


def _get_time_to_decelerate_outer_to_inner(context: _Steering) -> float:
    return (context.outer_speed - context.inner_speed)/_deceleration


def _equalize_speeds(context: _Steering) -> None:
    time_to_equalize = _get_time_to_equalize(context)
    middle_speed = context.middle_speed
    time_to_break = min(time_to_equalize, context.time_left)

    context.max_speed = max(context.max_speed - _deceleration*time_to_break, middle_speed)
    context.min_speed = min(context.min_speed + _acceleration*time_to_break, middle_speed)

    context.update(time_to_break)


def _accelerate_inner_to_outer(context: _Steering) -> None:
    time_to_accelerate = min(_get_time_to_accelerate_inner_to_outer(context), context.time_left)
    context.inner_speed = min(context.inner_speed + _acceleration*time_to_accelerate, context.outer_speed)
    context.update(time_to_accelerate)


def _decelerate_outer_to_inner(context: _Steering) -> None:
    time_to_decelerate = min(_get_time_to_decelerate_outer_to_inner(context), context.time_left)
    context.outer_speed = max(context.outer_speed - _deceleration*time_to_decelerate, context.inner_speed)
    context.update(time_to_decelerate)


class _AccelerationLimits(NamedTuple):
    max_outer_speed: float
    min_inner_speed: float


def _get_acceleration_limits(context: _Steering) -> _AccelerationLimits:
    max_outer_speed = min(_max_angular_speed*context.width + context.inner_speed, _max_speed)
    min_inner_speed = max(context.outer_speed - _max_angular_speed*context.width, _min_speed)

    return _AccelerationLimits(max_outer_speed, min_inner_speed)


def _get_time_to_accelerate(context: _Steering, limits: _AccelerationLimits) -> float:
    max_outer_change = limits.max_outer_speed - context.outer_speed
    max_inner_change = context.inner_speed - limits.min_inner_speed
    return max(max_outer_change/_acceleration, max_inner_change/_deceleration)


def _accelerate_rotation(context: _Steering, time_to_accelerate: float, limits: _AccelerationLimits) -> None:
    context.inner_speed = max(context.inner_speed - time_to_accelerate*_deceleration, limits.min_inner_speed)
    context.outer_speed = min(context.outer_speed + time_to_accelerate*_acceleration, limits.max_outer_speed)

    context.update(time_to_accelerate)


def _accelerate_linear(context: _Steering, dt: float) -> None:
    context.outer_speed = min(context.outer_speed + _acceleration*dt, _max_speed)
    context.inner_speed = min(context.inner_speed + _acceleration*dt, _max_speed)


def _break_linear(context: _Steering, dt: float) -> None:
    context.outer_speed = max(context.outer_speed - _deceleration*dt, 0)
    context.inner_speed = max(context.inner_speed - _deceleration*dt, 0)


def _steer_squadron(clock: Clock, squadron: Component, transform: TransformComponent, goal: _GoalInfo) -> _Steering:
    rotation_change_required = goal.want_rotation - transform.rotation

    while rotation_change_required > 180:
        rotation_change_required -= 360

    while rotation_change_required < -180:
        rotation_change_required += 360

    context = _Steering(
        squadron.breadth,
        rotation_change_required,
        squadron.left_speed,
        squadron.right_speed,
        clock.delta_time_s,
        goal
    )

    if context.rotation_left != 0:
        if context.wrong_direction:
            _equalize_speeds(context)

        if context.wrong_direction or context.time_left <= 0:
            return context

        acceleration_limits = _get_acceleration_limits(context)

        if context.angular_speed != 0:
            time_to_finish_rotation = max(
                math.radians(abs(rotation_change_required) - _acceleration_angle) / abs(context.angular_speed),
                0
            )

            time_to_equalize = _get_time_to_accelerate_inner_to_outer(context)
            time_to_accelerate = _get_time_to_accelerate(context, acceleration_limits)
            time_to_accelerate = max(min(time_to_finish_rotation - time_to_equalize, clock.delta_time_s), 0)
        else:
            time_to_accelerate = min(context.time_left, clock.delta_time_s)
            time_to_equalize = 0
            time_to_finish_rotation = 0

        _accelerate_rotation(context, time_to_accelerate, acceleration_limits)

        if time_to_equalize > time_to_finish_rotation:
            _accelerate_inner_to_outer(context)
        else:
            context.update(context.time_left)
    elif context.angular_speed != 0:
        if goal.want_speed > 0:
            _accelerate_inner_to_outer(context)
        else:
            _decelerate_outer_to_inner(context)

    if abs(context.rotation_left) <= _acceleration_angle:
        if goal.want_speed > goal.current_speed:
            _accelerate_linear(context, clock.delta_time_s)
        else:
            _break_linear(context, clock.delta_time_s)

    return context


def _update_formation(clock: Clock, squadron: Component, transform: TransformComponent) -> None:
    if squadron.rider_count == 0:
        return

    formation = squadron.formation

    _set_row_position_and_velocity(0, squadron, transform)

    for row in range(1, squadron.row_count):
        for column in range(squadron.column_count):
            forward_xy_v_r = formation.cell_to_xy_v_r[row - 1, column]
            xy_v_r = formation.cell_to_xy_v_r[row, column]

            forward_position = pygame.Vector2(*forward_xy_v_r[0])
            position = pygame.Vector2(*xy_v_r[0])

            direction = (position - forward_position).normalize()
            new_position = forward_position + direction*_formation_cell_size
            new_velocity = (new_position - position)/clock.delta_time_s

            rotation = normalize_angle(-pygame.Vector2(1, 0).angle_to(direction*-1))

            xy_v_r[0] = (new_position.x, new_position.y)
            xy_v_r[1] = (new_velocity.x, new_velocity.y)
            xy_v_r[2] = (rotation, 0)


def _get_goal(input: Input, current_velocity: pygame.Vector2, current_rotation: float) -> _GoalInfo:
    speed = current_velocity.length()

    want_rotation = current_rotation
    want_speed = speed

    if input.direction is None:
        want_speed = 0
    else:
        want_rotation = input.direction.angle

        if abs(want_rotation - current_rotation) <= _acceleration_angle:
            want_rotation = current_rotation

            if speed <= _max_speed:
                want_speed = min(speed + _acceleration, _max_speed)

    return _GoalInfo(speed, want_speed, want_rotation)


def _make_direction(rotation: float) -> pygame.Vector2:
    return pygame.Vector2(1, 0).rotate(-rotation)


class _HordeMember(Query):
    transform: TransformComponent
    boid_member: BoidMemberComponent


def _detect_enemy_collisions(
    storage: Storage,
    rider_entity: EntityHandle,
    rider: RiderComponent,
    rider_transform: TransformComponent
) -> None:
    rider_position = rider_transform.position

    # TODO: @without
    if storage.has_component(rider_entity, DeadComponent):
        return

    for enemy_transform, _ in storage.query(_HordeMember):
        enemy_position = enemy_transform.position
        if (enemy_position - rider_position).length_squared() < _collision_radius:
            storage.add_component(rider_entity, DeadComponent())
            return


_collision_radius = 40.0**2
_formation_cell_size = 64

_max_speed = 400.0
_min_speed = 0.0
_acceleration = 200.0  # 4 seconds to get to the max speed
_deceleration = 200.0  # 2 seconds to stop from the max speed
_max_rotation_rate = 45.0  # degrees in second, 2 seconds to do 90*
_max_angular_speed = math.pi*_max_rotation_rate/180.0
_acceleration_angle = 1.0

_max_depth = 5
_random_shift = (-5.0, 5.0)
