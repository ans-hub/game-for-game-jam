import asyncio
import pathlib

from typing import Any, Dict, List, NamedTuple, Type, TypeVar
from concurrent.futures import ThreadPoolExecutor

import loguru

from game.resource import Collection, Request
from game.resource.model import type_to_model_loader


logger = loguru.logger


_T = TypeVar('_T', bound=Collection)


class Loader:
    class _Batch(NamedTuple):
        requests: Dict[str, Request]
        collection: Collection

    def __init__(self, root: pathlib.Path) -> None:
        self.root = root
        self.ready = False
        self.total = 0
        self.loaded = 0

        self._batches: List[Loader._Batch] = []
        self._pool = ThreadPoolExecutor()

    def request(self, library_class: Type[_T]) -> _T:
        library_stub: _T = library_class()

        requests = library_class._requests
        batch = Loader._Batch(requests, library_stub)
        self._batches.append(batch)

        return library_stub

    def load(self, library_class: Type[_T]) -> _T:
        library_stub: _T = library_class()

        requests = library_class._requests
        for name, request in requests.items():
            resource = _load(self.root, request)
            library_stub._set(name, resource)

        return library_stub

    async def load_all(self):
        loop = asyncio.get_running_loop()

        for batch in self._batches:
            for name, request in batch.requests.items():
                resource = await loop.run_in_executor(self._pool, _load, self.root, request)
                batch.collection._set(name, resource)
                self.loaded += 1

        self.ready = True


def _load(root: pathlib.Path, request: Request) -> Any:
    model_loader = type_to_model_loader[request.type]

    path = root / model_loader.path_prefix / request.id
    return model_loader.load(path)
