#!/bin/sh

source .venv/bin/activate
python ./main.py  --fullscreen=true --width 1280 --height 720 "$@"
