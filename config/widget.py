import enum


class Anchor(enum.Enum):
    LeftTop = (0, 0)
    Center = (0.5, 0.5)
    MidBottom = (0.5, 1)
    RightBottom = (1, 1)

    def __init__(cls, x: float, y: float) -> None:
        cls.x = x
        cls.y = y


class VAlign(enum.Enum):
    Top = (0)
    Middle = (0.5)
    Bottom = (1)

    def __init__(cls, vert: float) -> None:
        cls.vert = vert


class HAlign(enum.Enum):
    Left = (0)
    Center = (0.5)
    Right = (1)

    def __init__(cls, hor: float) -> None:
        cls.hor = hor


class WidgetType(enum.Enum):
    Button = 1
    Caption = 2
    Image = 3
    AnimatedImage = 4

    @staticmethod
    def get(name: str) -> 'WidgetType':
        global _str_to_widget_type
        return _str_to_widget_type[name]


_str_to_widget_type = {
    "button": WidgetType.Button,
    "caption": WidgetType.Caption,
    "image": WidgetType.Image,
    "animated_image": WidgetType.AnimatedImage
}


class WidgetState(enum.Enum):
    Default = 1
    Pressed = 2

    @staticmethod
    def get(name: str) -> 'WidgetState':
        global _str_to_button_type
        return _str_to_button_type[name]


_str_to_button_type = {
    "default": WidgetState.Default,
    "pressed": WidgetState.Pressed
}


class FontFamily(enum.Enum):
    Arial = 1

    @staticmethod
    def get(name: str) -> 'FontFamily':
        global _str_to_font_family
        return _str_to_font_family[name]


_str_to_font_family = {
    "arial": FontFamily.Arial
}
