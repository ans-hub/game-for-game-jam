from recordclass import RecordClass
from typing import Any, Type
import typing


# For mypy
class _ResourceTypeStub:
    def __init__(self, model):
        pass


if typing.TYPE_CHECKING:
    class Request(Any):
        id: str
        type: Type
else:
    class Request(RecordClass):
        id: str
        type: Type = None
