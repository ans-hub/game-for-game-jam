import pygame
import asyncio
import pathlib
import sys

from asyncio.tasks import Task
from typing import NamedTuple

import argparse

import loguru

from game.core import Clock, FPSCounter, EventPump
from game.logic import Input, World
from game.render import Window, Camera
from game.resource import Loader
from game.sound.mixer import Mixer
from game.ui.layout import LayoutType

g_caption = "Voevoda Simulator 2021"
g_assets_root = pathlib.Path(sys.argv[0]).parent.absolute() / "assets"
g_fps_measurement_interval = 1000.0


def main() -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument('--width', type=int, default=1280, help='Window width')
    parser.add_argument('--height', type=int, default=1024, help='Window height')
    parser.add_argument('--fullscreen', type=bool, default=True, help='Run fullscreen')

    parser.add_argument('--max-fps', type=int, default=120, help='Limit FPS to N')
    parser.add_argument('--data', type=pathlib.Path, default=g_assets_root, help='Assets directory path')

    parser.add_argument('--start-rider-count', type=int, default=0, help='Spawn set about of riders at the start')

    parser.add_argument('--debug-draw', default=False, action='store_true', help='Enable debug draw')

    parser.add_argument('--skip-menu', default=False, action='store_true', help="Skip menu")

    args = parser.parse_args()

    return asyncio.run(_run(args))


async def _run(args) -> None:
    with _EngineInit():
        loader = Loader(args.data)

        window = Window(g_assets_root, args.width, args.height, g_caption, args.fullscreen)
        clock = Clock(args.max_fps)
        mixer = Mixer()

        game = _Game(loader, window, clock, mixer)

        load_task: Task = asyncio.create_task(loader.load_all())
        wait_task: Task = asyncio.create_task(game.wait_for_load(loader, args.skip_menu))

        await load_task
        await wait_task

        await game.run(args.start_rider_count, args.debug_draw)


class _Game:
    def __init__(self, loader: Loader, window: Window, clock: Clock, mixer: Mixer) -> None:
        self.event_pump = EventPump()

        self.window = window
        self.clock = clock
        self.mixer = mixer
        self.input = Input()
        self.world = World(loader, self.window.surface.get_rect())
        self.camera = Camera(self.window.surface.get_rect())

    async def wait_for_load(self, loader: Loader, skip_menu: bool) -> None:
        self.world.ui.activate_layout(LayoutType.Loading)

        while self.event_pump.is_running and not loader.ready:
            async with _Frame(self.clock, self.event_pump):
                self.window.clear()
                self.world.ui.update(self.clock, self.window, self.input)
                self.world.ui.draw(self.window)
                self.window.blit()

        if not skip_menu:
            self.world.ui.activate_layout(LayoutType.Menu)
        self.world.ui.deactivate_layout(LayoutType.Loading)

    async def run(self, start_rider_count: int, draw_debug: bool) -> None:
        fps_counter = FPSCounter(g_fps_measurement_interval)

        self.input.rider_count = start_rider_count
        self.input.debug_draw = draw_debug
        self.event_pump.add_key_handler(self.input)

        while self.event_pump.is_running:
            async with _Frame(self.clock, self.event_pump):
                self.camera.update()
                self.world.update(self.input, self.clock, self.camera, self.window, self.mixer)

                fps_counter.update(self.clock)

                self.window.clear()

                self.window.draw_text("{:02.0f}".format(fps_counter.fps), 0, 0)
                self.world.draw(self.clock, self.window, self.camera, self.input.debug_draw)

                self.input.update()
                self.window.blit()


class _Frame(NamedTuple):
    clock: Clock
    pump: EventPump

    async def __aenter__(self):
        await self.clock.update()
        self.pump.update()

    async def __aexit__(self, type, value, traceback):
        pygame.display.flip()


class _EngineInit:
    def __enter__(self):
        pygame.init()

    def __exit__(self, type, value, traceback):
        pygame.quit()


if __name__ == "__main__":
    if sys.settrace is None:
        main = loguru.logger.catch(main)

    main()
