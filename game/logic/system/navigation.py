from typing import Tuple, cast

import pygame
import pygame.draw
import pygame.transform

from recordclass import RecordClass

from game.ecs import Scheduler, Storage, env
from game.components import TotemComponent, TransformComponent
from game.render import Window, Camera
from game.logic.system import squadron

from game.resource import Collection, Loader, Request
from game.resource.model import UiWidget


def register_draw(storage: Storage, scheduler: Scheduler, loader: Loader) -> None:
    assets = loader.request(_Assets)
    env = _Env(assets)

    storage.add_env(env)

    scheduler.add(_draw_direction)


class _Assets(Collection):
    arrow_enemy: UiWidget = Request("icon_arrow_enemy_totem")
    arrow_player: UiWidget = Request("icon_arrow_player_totem")


@env
class _Env(RecordClass):
    assets: _Assets


def _draw_direction(
    storage: Storage,
    window: Window,
    camera: Camera,
    totem: TotemComponent,
    totem_transform: TransformComponent,
    env: squadron.Env,
    local_env: _Env
) -> None:
    totem_position = totem_transform.position

    if camera.is_in_bounds(totem_position):
        return

    assert env.squadron is not None
    position = storage.get_component(env.squadron, TransformComponent).position

    to_totem = totem_position - position
    distance_to_totem = to_totem.length()
    direction = to_totem / distance_to_totem

    assets = local_env.assets
    arrow = totem.is_enemy and assets.arrow_enemy or assets.arrow_player

    rotation = pygame.Vector2(1, 0).angle_to(direction)
    rotated_arrow = pygame.transform.rotate(arrow.surface, -rotation)

    origin = pygame.Vector2(window.scr_w/2, window.scr_h/2)
    far = origin + direction*max(window.scr_h, window.scr_w)*10.0

    camera_rect = pygame.Rect(0, 0, window.scr_w, window.scr_h)
    line = camera_rect.clipline(origin.x, origin.y, far.x, far.y)

    intersection = pygame.Vector2(*cast(Tuple[Tuple[int, int], Tuple[int, int]], line)[1])

    arrow_size = rotated_arrow.get_rect()

    if intersection.x >= (window.scr_w - arrow_size.w):
        intersection.x -= arrow_size.w

    if intersection.y >= (window.scr_h - arrow_size.h):
        intersection.y -= arrow_size.h

    window.surface.blit(rotated_arrow, (intersection.x, intersection.y))
