from game.components import CameraAnchorComponent, VelocityComponent
from game.ecs import Scheduler

from game.components import TransformComponent
from game.render.camera import Camera


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_update_camera_position)


_inertia_coefficient = 0.3


def _update_camera_position(
    camera: Camera,
    anchor: CameraAnchorComponent,
    transform: TransformComponent,
    velocity: VelocityComponent
) -> None:
    inertia_shift = velocity.velocity * _inertia_coefficient
    move = transform.position - camera.wpos + inertia_shift
    camera.move(move)
