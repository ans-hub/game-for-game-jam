from pathlib import Path
from typing import Any, Callable, Dict, NamedTuple, Type

from .character_sheet import CharacterSheet, load as character_sheet_load
from .tiled_map import TiledMap, load as tiled_map_load
from .sound import Sound, load as sound_load
from .ui_widget import UiWidget, load as ui_widget_load


class ModelLoader(NamedTuple):
    path_prefix: str
    load: Callable[[Path], Any]


type_to_model_loader: Dict[Type, ModelLoader] = {
    CharacterSheet: ModelLoader("characters", character_sheet_load),
    TiledMap: ModelLoader("map", tiled_map_load),
    Sound: ModelLoader("sounds", sound_load),
    UiWidget: ModelLoader("ui_widgets", ui_widget_load),
}


__all__ = (
    'AnimatedSprite',
    'TerrainTileSheet',
    'Map',
    'Sound',
    'Soundbank',
    'UiWidget',
)
