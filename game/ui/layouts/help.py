from game.resource.model.sound import Sound
from typing import NamedTuple
from game.ui.layout import Row
from game.ui.widgets import Caption, LoadingBar
from game.resource import Request, Collection
from game.ui import ui
from game.render import Window
from game.core import Clock
from game.resource.model.ui_widget import UiWidget
from game.resource.loader import Loader
from game.ui.widgets import Image, Button
from game.ui.layout import Row
from game.resource.loader import _load
from config.widget import Anchor, VAlign, HAlign
from game.ecs import component
from recordclass.typing import RecordClass
from game.ui.layout import Row, LayoutType


class _Assets(Collection):
    img_menu: UiWidget = Request("img_menu")
    img_help: UiWidget = Request("img_help")
    btn_menu: UiWidget = Request("btn_menu")


_data: _Assets


class HelpLayout(ui.Layout):
    def __init__(self, loader: Loader) -> None:
        super().__init__(loader)

        global _data
        _data = loader.load(_Assets)

        self.type = ui.LayoutType.Help
        self.img_help: Image = Image("img_help", assets=_data)
        self.img_menu: Image = Image("img_menu", assets=_data)
        self.btn_menu: Button = Button("btn_menu", lambda: self._change_layout(LayoutType.Help, LayoutType.Menu), _data)

    def update(self, clock: Clock, window: Window) -> None:
        global _data

        super().update(clock, window)

        self._add_rows(
            Row(
                self.img_menu,
                height=self.window_rect.h,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=20
        )
        self._add_rows(
            Row(
                self.img_help,
                height=window.scr_h // 2,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=40
        )
        self._add_rows(
            Row(
                self.btn_menu,
                height=window.scr_h // 2,
                valign=VAlign.Bottom, halign=HAlign.Center
            ),
            valign=VAlign.Bottom, margin=40
        )
