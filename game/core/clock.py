import asyncio
import pygame as pg
import timeit

from concurrent.futures import ThreadPoolExecutor


class Clock:
    def __init__(self, max_fps: int) -> None:
        self.clock = pg.time.Clock()
        self.min_frame_time_ms: float = 1000.0 / max_fps
        self.frame_count = 0
        self.delta_time: float = 0.0
        self.delta_time_s: float = 0.0
        self.total_time: float = 0.0
        self.total_time_s: float = 0.0
        self._pool = ThreadPoolExecutor()

    async def update(self) -> None:
        # We are not using the SDL delay thing, so we can run other coroutines in meantime
        raw_tick_ms = self.clock.tick()

        delay_ms = max(0, self.min_frame_time_ms - raw_tick_ms)
        sleep_start = timeit.default_timer()
        await asyncio.sleep(delay_ms / 1000.0)
        sleep_stop = timeit.default_timer()
        sleep_duration = (sleep_stop - sleep_start)*1000.0

        self.delta_time = raw_tick_ms + sleep_duration
        self.delta_time_s = self.delta_time / 1000.0
        self.total_time += self.delta_time
        self.total_time_s += self.delta_time_s
        self.frame_count += 1


class FPSCounter:
    def __init__(self, measurement_interval: float) -> None:
        self.fps = 0.0

        self._measurement_interval = measurement_interval
        self._previous_total_time = 0.0
        self._previous_frame_count = 0

    def update(self, clock: Clock) -> None:
        interval_s = clock.total_time - self._previous_total_time

        if interval_s < self._measurement_interval:
            return

        frame_count = clock.frame_count - self._previous_frame_count

        self.fps = frame_count * 1000 / interval_s
        self._previous_frame_count = clock.frame_count
        self._previous_total_time = clock.total_time
