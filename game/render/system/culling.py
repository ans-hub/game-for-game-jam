from recordclass.typing import RecordClass

from game.ecs import component, Scheduler
from game.components import TransformComponent

from game.render import Camera


@component
class Component(RecordClass):
    visible: bool = True


def register_update(scheduler: Scheduler):
    scheduler.add(_update)


def _update(culling: Component, camera: Camera, transform: TransformComponent) -> None:
    in_bounds = camera.is_in_bounds(transform.position)
    culling.visible = in_bounds
