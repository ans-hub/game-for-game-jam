
import pygame

from recordclass.typing import RecordClass

from game.ecs import Storage, EntityHandle, env

from game.components import BoidMemberComponent, EnemyTotemComponent, PlayerTotemComponent, TotemComponent
from game.components import SpawnedComponent, TransformComponent, VelocityComponent
from game.resource import Request, Loader, Collection
from game.resource.model import CharacterSheet, Sound

from game.logic.system import squadron, shooting, sound
from game.render.system import culling, character_animation, sprite_draw


def init(storage: Storage, loader: Loader) -> None:
    assets = loader.request(_Assets)
    env = _Env(assets)
    storage.add_env(env)


def spawn_rider(storage: Storage) -> EntityHandle:
    env = storage.get_env(_Env)

    entity = storage.create_entity()

    storage.add_component(entity, SpawnedComponent())
    storage.add_component(entity, squadron.RiderComponent())
    storage.add_component(entity, shooting.BowOwner())
    storage.add_component(entity, sound.Component(env.assets.hooves_sound))

    _add_base_character_components(storage, entity, pygame.Vector2(0, 0), env.assets.archer)

    return entity


def spawn_ghost(storage: Storage, position: pygame.Vector2, boid_id: int) -> EntityHandle:
    env = storage.get_env(_Env)

    entity = storage.create_entity()

    storage.add_component(entity, SpawnedComponent())
    storage.add_component(entity, BoidMemberComponent(boid_id))

    _add_base_character_components(storage, entity, position, env.assets.ghost)

    return entity


def spawn_totem(
    storage: Storage,
    position: pygame.Vector2,
    is_enemy: bool = True,
    souls: int = 10,
    spawn_cooldown: float = 20.0,
    health: int = 100
) -> EntityHandle:
    env = storage.get_env(_Env)

    entity = storage.create_entity()

    storage.add_component(entity, TotemComponent(
        is_enemy=is_enemy,
        souls=souls,
        spawn_cooldown=spawn_cooldown,
        health=health,
        original_health=health
    ))

    if is_enemy:
        asset = env.assets.totem_enemy
        storage.add_component(entity, EnemyTotemComponent())
    else:
        asset = env.assets.totem_player
        storage.add_component(entity, PlayerTotemComponent())

    _add_base_character_components(storage, entity, position, asset, z_level=1)

    return entity


def _add_base_character_components(
    storage: Storage,
    entity: EntityHandle,
    position: pygame.Vector2,
    character: CharacterSheet,
    z_level: int = 0
) -> None:
    storage.add_component(entity, TransformComponent(position, z_level=z_level))
    storage.add_component(entity, culling.Component())
    storage.add_component(entity, VelocityComponent())
    storage.add_component(entity, sprite_draw.Component(character.surfaces[0], character.meta.rects[0]))
    storage.add_component(entity, character_animation.Component(character.surfaces, character.meta))


class _Assets(Collection):
    hooves_sound: Sound = Request("hooves")
    archer: CharacterSheet = Request("archer")
    ghost: CharacterSheet = Request("ghost")
    totem_player: CharacterSheet = Request("totem_player")
    totem_enemy: CharacterSheet = Request("totem_enemy")


@env
class _Env(RecordClass):
    assets: _Assets
