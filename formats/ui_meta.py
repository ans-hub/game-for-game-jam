from formats.common import DiskNamedTuple
from typing import Tuple, Optional, Dict


class _DiskTextMeta(DiskNamedTuple):
    text_offset: Tuple[int, int]
    text_size: int
    font_family: str


class _DiskWidgetStateMeta(DiskNamedTuple):
    tile_coord: Tuple[int, int]


class Meta(DiskNamedTuple):
    type: str
    sheet_size: Tuple[int, int]
    sheet_offset: Tuple[int, int]
    tile_size: Tuple[int, int]

    scale: Optional[float] = 1.0
    text: Optional[_DiskTextMeta] = None
    named_tiles: Optional[Dict[str, _DiskWidgetStateMeta]] = None
