import pathlib
import sys
import copy
import math

from typing import Dict, Iterable, List, NamedTuple, Optional, Tuple, Union

import bpy
import typing_json

from PIL import Image

# HACK: the whole selection logic can be moved outside of the blender
root = pathlib.Path(__file__).parent.parent.parent

sys.path.append(root.as_posix())

from config import LowerAction, UpperAction, Direction, TileSize # noqa
import formats.character_meta as format # noqa


_camera: str = 'Camera_Root'
_angle_count = len(Direction)
_output_width = 4096
_output_path = root / 'assets' / 'characters'


class Pose(NamedTuple):
    action: str
    pose: str


class Animation(NamedTuple):
    name: str


class AnimationSet:
    def __init__(self, *variants: str) -> None:
        self.variants = [Animation(name) for name in variants]


class UpperActionSelector(NamedTuple):
    # Cyclic types are not support by mypy atm
    selection: Dict[UpperAction, Union[AnimationSet, Pose, 'LowerActionSelector']]  # type: ignore


class LowerActionSelector(NamedTuple):
    selection: Dict[LowerAction, Union[AnimationSet, Pose, UpperActionSelector]]


class SpriteInfo(NamedTuple):
    path: pathlib.Path
    frame: int
    angle: int


class SheetInfo(NamedTuple):
    lower_action: LowerAction
    upper_action: UpperAction
    variant: int
    frame_count: int
    sprites: List[SpriteInfo]


class _Variant(NamedTuple):
    armature_to_modifier: Dict[str, Union[Animation, Pose]]


class Config(NamedTuple):
    layer: str

    frames_per_sprite: int
    sprite_size: int

    render_resolution: Tuple[int, int]

    animation_tree: Dict[str, Union[LowerActionSelector, UpperActionSelector]]

    lower_action_aliases: Dict[LowerAction, LowerAction] = {}

    lower_actions: Iterable[LowerAction] = LowerAction
    upper_actions: Iterable[UpperAction] = UpperAction

    looped_actions: Dict[LowerAction, bool] = {}


def export(config: Config, quit: bool = True) -> None:
    try:
        _export(config)
    except Exception:
        quit = True
        raise
    finally:
        if quit:
            bpy.ops.wm.quit_blender()


def _export(config: Config) -> None:
    camera = bpy.data.objects[_camera]

    angle_step = 360 // _angle_count
    assert angle_step * _angle_count == 360

    frame_length = (1000 / bpy.context.scene.render.fps) * config.frames_per_sprite

    sheets: List[SheetInfo] = []

    for lower_action in config.lower_actions:
        for upper_action in config.upper_actions:
            variants = _gather_variants(config, lower_action, upper_action)

            for variant_i, variant in enumerate(variants):
                frame_count = _get_frame_count(variant, lower_action, upper_action)
                sprite_count = frame_count // config.frames_per_sprite

                print(frame_count)

                assert sprite_count * config.frames_per_sprite == frame_count, (
                    "Can't separate animation into discreet frames. Action: {}, frame count: {}"
                    .format(lower_action.id, frame_count)
                )

                sheet = SheetInfo(lower_action, upper_action, variant_i, sprite_count, [])

                for frame in range(0, frame_count, config.frames_per_sprite):
                    for angle in range(0, 360, angle_step):
                        camera.rotation_euler = [0, 0, -math.radians(angle)]

                        path = _render(config, variant, lower_action, upper_action, variant_i, frame, angle)
                        sheet.sprites.append(SpriteInfo(path, frame, angle))

                sheets.append(sheet)

    _merge_spritesheets(config, sheets, frame_length)


def _gather_variants(config: Config, lower_action: LowerAction, upper_action: UpperAction) -> List[_Variant]:
    armature_to_selection_variants: Dict[str, Union[List[Animation], List[Pose]]] = {}

    for armature, selection in config.animation_tree.items():
        selection_variants = _select_variants(selection, lower_action, upper_action)

        armature_to_selection_variants[armature] = selection_variants

    armatures = list(armature_to_selection_variants.keys())

    variants: List[_Variant] = []

    def _fill_variants(i: int, variant: Dict[str, Union[Animation, Pose]]) -> None:
        if i == len(armatures):
            variants.append(_Variant(variant))
        else:
            armature = armatures[i]

            for selection_variant in armature_to_selection_variants[armature]:
                subvariant = copy.copy(variant)
                subvariant[armature] = selection_variant

                _fill_variants(i + 1, subvariant)

    _fill_variants(0, {})

    return variants


def _select_variants(
    selection: Union[LowerActionSelector, UpperActionSelector, AnimationSet, Pose],
    lower_action: LowerAction,
    upper_action: UpperAction
) -> Union[List[Animation], List[Pose]]:
    if isinstance(selection, Pose):
        return [selection]
    elif isinstance(selection, AnimationSet):
        return selection.variants
    elif isinstance(selection, UpperActionSelector):
        return _select_variants(selection.selection[upper_action], lower_action, upper_action)
    elif isinstance(selection, LowerActionSelector):
        return _select_variants(selection.selection[lower_action], lower_action, upper_action)


def _render(
    config: Config,
    variant: _Variant,
    lower_action: LowerAction,
    upper_action: UpperAction,
    variant_i: int,
    frame: int,
    angle: int
) -> pathlib.Path:
    _set_frame(frame)
    _apply_variant(variant)

    scene = bpy.context.scene
    temp = pathlib.Path(bpy.app.tempdir)

    armature_name = "{}_{}_{}_{}_{}_{}".format(config.layer, lower_action.id, upper_action.id, variant_i, frame, angle)
    path = (temp / armature_name).with_suffix(scene.render.file_extension)
    scene.render.filepath = path.as_posix()

    scene.render.resolution_x = config.render_resolution[0]
    scene.render.resolution_y = config.render_resolution[1]

    bpy.ops.render.render(write_still=True, layer=config.layer)

    return path


def _apply_variant(variant: _Variant) -> None:
    for armature_name, modifier in variant.armature_to_modifier.items():
        if isinstance(modifier, Pose):
            _set_pose(armature_name, modifier)
        else:
            _set_action(armature_name, modifier)


# TODO: this can theoretically be done from outside of blender
# TODO: we can pack different sheets into one image for potential opengl optimization
def _merge_spritesheets(config: Config, sheets: List[SheetInfo], frame_length: float) -> None:
    column_count = _output_width // config.sprite_size

    files: List[str] = []
    sheet_metas: Dict[str, Dict[str, List[format.Sheet]]] = {}

    for sheet in sheets:
        row_count = max(int(math.ceil(len(sheet.sprites) / column_count)), 1)
        output_height = row_count * config.sprite_size

        sheet_image = Image.new('RGBA', (_output_width, output_height))

        row = 0
        column = 0

        starts: Dict[Direction, int] = {}

        sheet.sprites.sort(key=lambda x: (x.angle, x.frame))

        for i, info in enumerate(sheet.sprites):
            sprite = Image.open(info.path)
            sprite = sprite.resize((config.sprite_size, config.sprite_size))

            destination = (column * config.sprite_size, row * config.sprite_size)

            sheet_image.paste(sprite, destination)

            column += 1

            if column >= column_count:
                row += 1
                column = 0

            if info.frame == 0:
                starts[Direction(info.angle)] = i

        assert row <= row_count

        name = "{}_{}_{}_{}.png".format(
            config.layer.lower(), sheet.lower_action.id, sheet.upper_action.id, sheet.variant
        )

        with open(_output_path / name, 'wb') as f:
            sheet_image.save(f, bitmap_format='png')

        file_index = len(files)
        files.append(name)

        variants = sheet_metas.setdefault(sheet.lower_action.id, {}).setdefault(sheet.upper_action.id, [])

        looped = (
            sheet.lower_action.looped
            or sheet.upper_action.looped
            or config.looped_actions.get(sheet.lower_action, False)
        )

        ordered_starts = [starts[direction] for direction in Direction]
        # HACK: some mypy bugs here, it just reports random stuff
        variants.append(format.Sheet(file_index, sheet.frame_count, looped, ordered_starts))  # type: ignore

    size_in_tiles = (config.sprite_size // TileSize.w, config.sprite_size // TileSize.h)

    lower_action_aliases = {action.id: alias.id for action, alias in config.lower_action_aliases.items()}

    meta = format.Main(size_in_tiles, frame_length, files, sheet_metas, lower_action_aliases)  # type: ignore

    output_meta_path = _get_output_meta_path(config)

    with open(output_meta_path, 'w') as meta_file:
        typing_json.dump(meta, format.Main, meta_file, indent=4)


def _get_frame_count(variant: _Variant, lower_action: LowerAction, upper_action: UpperAction) -> int:
    frame_count: Optional[int] = None

    for modifier in variant.armature_to_modifier.values():
        if isinstance(modifier, Pose):
            continue

        action = bpy.data.actions[modifier.name]
        assert action.frame_range[0] == 0

        animation_frame_count = int(action.frame_range[-1])

        if frame_count is None:
            frame_count = animation_frame_count
        else:
            assert frame_count == animation_frame_count, (
                "Variant animations frame count mismatch.\n" +
                "Lower: {}, upper: {}, animation: {}, total frame count: {}, animation frame count: {}"
            ).format(lower_action, upper_action, modifier.name, frame_count, animation_frame_count)

    assert frame_count is not None

    return frame_count


def _set_frame(index: int) -> None:
    bpy.context.scene.frame_set(index)


def _set_action(armature_name: str, animation: Animation) -> None:
    armature = bpy.data.objects[armature_name]
    action = bpy.data.actions[animation.name]
    armature.animation_data.action = action


def _find_pose(armature: bpy.types.Object, pose_name: str) -> Optional[int]:
    index = 0
    for marker in armature.pose_library.pose_markers:
        if pose_name == marker.name:
            return index
        index += 1
    return None


def _set_pose(armature_name: str, pose: Pose) -> None:
    armature = bpy.data.objects[armature_name]
    bpy.context.view_layer.objects.active = armature

    armature.pose_library = bpy.data.actions[pose.action]

    for bone in armature.data.bones:
        bone.select = True

    index = _find_pose(armature, pose.pose)

    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='SELECT')
    bpy.ops.poselib.apply_pose(pose_index=index)


def _get_output_meta_path(config: Config):
    return (_output_path / config.layer.lower()).with_suffix('.meta')
