from typing import Optional

from recordclass import RecordClass

import pygame

from game.ecs import component


@component
class CameraAnchorComponent:
    pass


@component
class PlayerTotemComponent:
    pass


@component
class EnemyTotemComponent:
    pass


@component
class TotemComponent(RecordClass):
    spawn_cooldown: float = 25.0
    souls: int = 0
    is_enemy: bool = True
    spawn_radius: float = 32
    spawn_last_time: float = 0.0

    health: int = 100
    original_health: int = 100


@component
class BoidMemberComponent(RecordClass):
    ownerBoidId: int = 0


@component
class VelocityComponent:
    velocity: pygame.Vector2

    def __init__(self, velocity: Optional[pygame.Vector2] = None):
        if velocity is None:
            self.velocity = pygame.Vector2(0, 0)
        else:
            self.velocity = velocity


@component
class TransformComponent:
    def __init__(self, position: Optional[pygame.Vector2] = None, rotation: float = 0.0, z_level: int = 0):
        if position is None:
            self.position = pygame.Vector2(0, 0)
        else:
            self.position = position

        self.rotation = rotation
        self.z_level = z_level


@component
class SpawnedComponent:
    pass


@component
class DeadComponent:
    pass
