from typing import Callable
from game.resource import Request, Collection
from game.ui import ui
from game.render import Window
from game.resource.model.ui_widget import UiWidget
from game.resource.loader import Loader
from game.ui.widgets import Button, Image
from game.ui.layout import LayoutType, Row
from game.core import Clock
from config.widget import VAlign, HAlign


class _Assets(Collection):
    img_menu: UiWidget = Request("img_menu")
    btn_start: UiWidget = Request("btn_start")
    btn_end: UiWidget = Request("btn_end")
    btn_help: UiWidget = Request("btn_help")


_data: _Assets


class MenuLayout(ui.Layout):
    def __init__(self, loader: Loader, start_game_callback: Callable) -> None:
        super().__init__(loader)

        global _data
        _data = loader.request(_Assets)

        self.type = ui.LayoutType.Menu
        self.img_background: Image = Image("img_menu", _data)
        self.btn_start: Button = Button(
            "btn_start",
            lambda: self._start_or_resume(start_game_callback),
            _data
        )
        self.btn_help: Button = Button(
                "btn_help",
                lambda: self._change_layout(LayoutType.Menu, LayoutType.Help),
                _data
        )
        self.btn_end: Button = Button("btn_end", lambda: exit(), _data)

    def _start_or_resume(self, start_game_callback: Callable) -> None:
        start_game_callback()
        self._change_layout(LayoutType.Menu, LayoutType.Hud)

    def update(self, clock: Clock, window: Window) -> None:
        global _data

        super().update(clock, window)

        self._add_rows(
            Row(
                self.img_background,
                height=self.window_rect.h,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=20
        )
        self._add_rows(
            Row(
                self.btn_start,
                height=0,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            Row(
                self.btn_help,
                height=0,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            Row(
                self.btn_end,
                height=0,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=0
        )
