from typing import Any, Dict, Type, cast
import typing

from game.resource.request import Request


class _CollectionMeta(type):
    def __new__(cls, name, bases, attributes) -> '_CollectionMeta':
        requests = _take_requests(attributes)

        class_ = cast(_CollectionMeta, super().__new__(cls, name, bases, attributes))

        annotations = typing.get_type_hints(class_)
        _assign_types(requests, annotations)

        class_._requests = requests  # type: ignore

        return class_


def _take_requests(attributes: Dict[str, Any]) -> Dict[str, Request]:
    requests: Dict[str, Request] = {}

    for name, request in attributes.items():
        if name.startswith('_'):
            continue

        request = attributes[name]
        assert isinstance(request, Request), "Library member {} is not a request".format(name)

        requests[name] = request

    for name in requests.keys():
        del attributes[name]

    return requests


def _assign_types(requests: Dict[str, Request], annotations: Dict[str, Type]) -> None:
    for name, request in requests.items():
        request.type = annotations[name]


class Collection(metaclass=_CollectionMeta):
    """
        Usage:
        ```
            class Something:
                class Assets(resource.Library):
                    # Will be looked as 'spritesheets/something' + '.png'/'.meta'
                    sprite: resource.Sprite = resource.Request("something")

                def __init__(self, resource_manager):
                    # Creates stub object that will be initialized deferredly
                    self.assets = resource_manager.request(Assets)

                def draw(self, window):
                    self.assets.sprite.draw(window)
        ```

        Do not add anything else, it's not super-robust against misuse
    """

    _requests: Dict[str, Request]

    def __init__(self) -> None:
        for name in self._requests:
            self.__dict__[name] = None

    def _set(self, name: str, resource: Any) -> None:
        self.__dict__[name] = resource
