import math
from typing import List, Tuple

import pygame

from config import TileSize

from game import resource
from game.core import Position
from game.render.window import Window
from game.render.camera import Camera

import game.resource.model.tiled_map as model


class Terrain:
    class Assets(resource.Collection):
        map: model.TiledMap = resource.Request("random")

    def __init__(self, loader: resource.Loader, window_size: pygame.Rect) -> None:
        self._assets = loader.request(Terrain.Assets)

        self._count_w = int(math.ceil(window_size.width / TileSize.w)) + 4
        self._count_h = int(math.ceil(window_size.height / _half_tile.h)) + 4

        self._surface = pygame.Surface((window_size.width + TileSize.w*2, window_size.height + TileSize.h*2))
        self._top_left_visible = Position(0, 0)

    @property
    def objects(self) -> List[model.Object]:
        return self._assets.map.objects

    def update(self, camera: Camera) -> None:
        camera_top_left = Position(
            int(camera.left // TileSize.width) * TileSize.width,
            int(camera.top // TileSize.height) * TileSize.height
        )

        if camera_top_left == self._top_left_visible:
            return

        self._top_left_visible = camera_top_left

        def _get_blit_spec(x_i: int, y_i: int) -> Tuple[pygame.Surface, pygame.Rect, pygame.Rect]:
            if y_i % 2 == 0:
                screen_x = x_i*TileSize.w
                screen_y = y_i*_half_tile.h
            else:
                screen_x = x_i*TileSize.w + _half_tile.w
                screen_y = y_i*_half_tile.h
            sceen_position = Position(screen_x, screen_y)
            world_position = camera_top_left + sceen_position
            grid_position = self._to_grid(world_position)
            tile = self._get_tile(grid_position)
            return (
                tile.surface,
                TileSize.move(sceen_position.x, sceen_position.y),
                tile.rect
            )

        surfaces = [
            _get_blit_spec(x_i, y_i)
            for y_i in range(self._count_h - 1, -2, -1)
            for x_i in range(self._count_w - 1, -2, -1)
        ]
        self._surface.blits(surfaces, doreturn=False)  # type: ignore

    def draw(self, window: Window, camera: Camera):
        shift_x = self._top_left_visible.x - camera.left
        shift_y = self._top_left_visible.y - camera.top
        window.surface.blit(self._surface, (shift_x, shift_y))

    def _to_grid(self, world: Position) -> Position:
        x = world.x // TileSize.w + world.y // TileSize.h
        y = world.y // TileSize.h - world.x // TileSize.w

        return Position(x, y)

    def _get_tile(self, grid_position: Position) -> model.TerrainTile:
        tiles = self._assets.map.tiles
        grid = self._assets.map.grid

        if grid_position in grid:
            tile_index = grid.get(grid_position)
        else:
            tile_index = grid.get(Position(0, 0))

        return tiles[tile_index]


_half_tile = pygame.Rect(0, 0, TileSize.w // 2, TileSize.h // 2)
