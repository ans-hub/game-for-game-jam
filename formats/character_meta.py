from typing import Dict, List, Tuple
from formats.common import DiskNamedTuple


class Sheet(DiskNamedTuple):
    file_index: int
    frame_count: int
    looped: bool

    starts_per_direction: List[int]


class Main(DiskNamedTuple):
    size: Tuple[int, int]
    frame_length: float
    files: List[str]

    sheets: Dict[str, Dict[str, List[Sheet]]]
    lower_action_aliases: Dict[str, str]
