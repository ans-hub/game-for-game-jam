import pygame

import numpy as np

from game.ecs import EntityHandle


assert EntityHandle.InvalidValue == 0


class Entity:
    def __init__(self, width: int, height: int) -> None:
        self._grid = np.zeros((width, height), dtype=EntityHandle.Type)

    def is_empty(self, position: pygame.Vector2) -> bool:
        return self._grid[position.x, position.y] == 0

    def get(self, position: pygame.Vector2) -> EntityHandle:
        return EntityHandle(self._grid[position.x, position.y])

    def set(self, position: pygame.Vector2, entity: EntityHandle) -> None:
        self._grid[position.x, position.y] = entity.value

    def clear(self, position: pygame.Vector2) -> None:
        self._grid[position.x, position.y] = EntityHandle.InvalidValue
