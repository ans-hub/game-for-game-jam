from typing import Any


def swap_elements(_list: Any, i: int, k: int) -> None:
    _list[i], _list[k] = _list[k], _list[i]


def normalize_angle(angle):
    while angle < 0.0:
        angle += 360.0

    while angle > 360.0:
        angle -= 360.0

    return angle
