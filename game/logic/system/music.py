from typing import Optional
import pygame.mixer

import random

from recordclass import RecordClass

from game.ecs import env, Storage, Scheduler

from game.resource import Collection, Request, Loader
from game.resource.model import Sound

from game.sound import Mixer


class _Assets(Collection):
    music: Sound = Request("bg_battle")


@env
class Env(RecordClass):
    assets: _Assets
    channel: Optional[pygame.mixer.Channel] = None


def register_update(storage: Storage, scheduler: Scheduler, loader: Loader) -> None:
    assets = loader.request(_Assets)
    env = Env(assets)

    storage.add_env(env)
    scheduler.add(_update_music)


def _update_music(env: Env, mixer: Mixer) -> None:
    if env.channel is None or not env.channel.get_busy():
        sound = random.choice(env.assets.music.sounds)

        if env.channel is not None:
            mixer.stop(env.channel)

        env.channel = mixer.play(sound, is_music=True)
