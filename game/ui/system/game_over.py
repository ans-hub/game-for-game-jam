from game.ui.layout import LayoutType
from game.components import EnemyTotemComponent, PlayerTotemComponent, TotemComponent
from game.ecs import Scheduler, Query, Storage
from game.ui import Ui
from game.logic.system import squadron


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_check_victory)
    scheduler.add(_check_loss)


class _PlayerTotems(Query):
    totem: TotemComponent
    is_player: PlayerTotemComponent


def _check_loss(storage: Storage, ui: Ui, squadron_: squadron.Component) -> None:
    if squadron_.rider_count > 0:
        return

    soul_count = 0

    for totem, _ in storage.query(_PlayerTotems):
        soul_count += totem.souls

    if soul_count > 0:
        return

    ui.activate_layout(LayoutType.GameOver)


class _EnemyTotems(Query):
    totem: TotemComponent
    is_enemy: EnemyTotemComponent


def _check_victory(storage: Storage, ui: Ui) -> None:
    for _ in storage.query(_EnemyTotems):
        return

    ui.activate_layout(LayoutType.Victory)
