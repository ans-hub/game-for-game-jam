from typing import Callable, List
import pygame
import pygame.draw

from game.render.window import Window
from game.render.camera import Camera


class DebugDraw:
    def __init__(self) -> None:
        self._calls: List[Callable[[Window, Camera], pygame.Rect]] = []

    def circle(self, color: pygame.Color, position: pygame.Vector2, radius: float) -> None:
        self._calls.append(lambda w, c: pygame.draw.circle(w.surface, color, position - c.top_left, radius))

    def line(self, color: pygame.Color, start: pygame.Vector2, end: pygame.Vector2, width: int) -> None:
        self._calls.append(
            lambda w, c: pygame.draw.line(w.surface, color, start - c.top_left, end - c.top_left, width)
        )

    def text(self, txt: str, position: pygame.Vector2) -> None:
        self._calls.append(
            lambda w, c: w.draw_text(txt, int(position.x - c.top_left.x), int(position.y - c.top_left.y))
        )

    def draw(self, window: Window, camera: Camera) -> None:
        for call in self._calls:
            call(window, camera)

        self.clear()

    def clear(self) -> None:
        self._calls.clear()
