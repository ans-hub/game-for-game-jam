import pygame


class Color:
    Black = pygame.Color(0, 0, 0)
    White = pygame.Color(255, 255, 255)
    Red = pygame.Color(255, 0, 0)
    Green = pygame.Color(0, 255, 0)
    Blue = pygame.Color(0, 0, 255)
    Yellow = pygame.Color(229, 201, 60)
    Transparent = pygame.Color(0, 0, 0, 0)
