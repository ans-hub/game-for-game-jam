import pygame
import random

from recordclass.typing import RecordClass

from game.ecs import component, Storage, Scheduler, EntityHandle, Query

from game.core import Clock, Color
from game.render.system import culling
from game.render.camera import Camera
from game.components import BoidMemberComponent, EnemyTotemComponent, TotemComponent, TransformComponent
from game.render.primitive_draw import PrimitiveDraw
from game.render import DebugDraw
from game.logic.system import squadron
from game.logic.input import Input


_deceleration = -5.0
_start_speed = 1200.0
_collision_radius_squared = 40.0**2
_half_cone_angle = 60.0
_dispersion_angle = (-10.0, 10.0)
_dispersion_distance = (-50.0, 50.0)
_shoot_cooldown = 0.5
_show_failure_duration = 0.1


@component
class ArrowComponent(RecordClass):
    current_speed: float = 0.0
    direction: pygame.Vector2 = pygame.Vector2(0, 0)
    distance: float = 0.0


@component
class ArrowDrawComponent:
    back_pt: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    front_pt: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tip_1: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tip_2: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_1: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_2: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_3: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_4: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_5: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_6: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_start_1: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_start_2: pygame.Vector2 = pygame.Vector2(0.0, 0.0)
    tail_start_3: pygame.Vector2 = pygame.Vector2(0.0, 0.0)


@component
class BowOwner:
    time_last_shot: float = 0.0
    click_position: pygame.Vector2 = pygame.Vector2(0, 0)
    failed_to_shoot_at: float = 0.0


def register_debug_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw_click)


def _draw_click(debug_draw: DebugDraw, bow_owner: BowOwner) -> None:
    debug_draw.circle(Color.White, bow_owner.click_position, 40.0)


def create_arrow(storage: Storage, position: pygame.Vector2, rotation: float, distance: float) -> EntityHandle:
    entity = storage.create_entity()

    direction = _make_direction(rotation)

    storage.add_component(entity, TransformComponent(position, rotation))
    storage.add_component(entity, culling.Component())
    storage.add_component(entity, ArrowComponent(_start_speed, direction, distance))
    storage.add_component(entity, create_arrow_draw_component(-rotation))

    return entity


def try_shoot(storage: Storage, from_pos: pygame.Vector2, to_pos: pygame.Vector2, shooter_rotation: float) -> bool:
    target_direction = to_pos - from_pos
    shooter_direction = _make_direction(shooter_rotation)
    shoot_angle = shooter_direction.angle_to(target_direction)

    while shoot_angle > 180:
        shoot_angle -= 360

    while shoot_angle < -180:
        shoot_angle += 360

    if abs(shoot_angle) > _half_cone_angle:
        return False

    rotation = -pygame.Vector2(1, 0).angle_to(target_direction) + random.uniform(*_dispersion_angle)
    distance = target_direction.length() + random.uniform(*_dispersion_distance)
    create_arrow(storage, from_pos, rotation, distance)
    return True


def _shooting_update(storage: Storage, clock: Clock, input: Input, camera: Camera,
                     bow_owner: BowOwner, transform: TransformComponent) -> None:
    if not input.is_lmb_clicked():
        return

    if clock.total_time_s - bow_owner.time_last_shot < _shoot_cooldown:
        return

    shoot_to = input.mouse_position + camera.top_left

    bow_owner.click_position = shoot_to
    if try_shoot(storage, transform.position, shoot_to, transform.rotation):
        bow_owner.time_last_shot = clock.total_time_s
    else:
        bow_owner.failed_to_shoot_at = clock.total_time_s


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_shooting_update)
    scheduler.add(_update_arrow_movement)
    scheduler.add(_detect_collisions)


def register_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw_arrow)
    scheduler.add(_draw_failures)


def create_arrow_draw_component(rotation: float) -> ArrowDrawComponent:
    arrow_draw = ArrowDrawComponent()

    _arrow_length = 50.0
    _arrow_tip = 7.0
    _tail_offset = 5.0

    back_pt = pygame.Vector2(-_arrow_length * 0.5, 0.0)
    front_pt = pygame.Vector2(_arrow_length * 0.5, 0.0)
    tip_1 = pygame.Vector2(_arrow_length * 0.5 - _arrow_tip, _arrow_tip)
    tip_2 = pygame.Vector2(_arrow_length * 0.5 - _arrow_tip, -_arrow_tip)
    tail_1 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip - _tail_offset, _arrow_tip)
    tail_2 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip, _arrow_tip)
    tail_3 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip + _tail_offset, _arrow_tip)
    tail_4 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip - _tail_offset, -_arrow_tip)
    tail_5 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip, -_arrow_tip)
    tail_6 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip + _tail_offset, -_arrow_tip)
    tail_start_1 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip, 0.0)
    tail_start_2 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip + _tail_offset, 0.0)
    tail_start_3 = pygame.Vector2(-_arrow_length * 0.5 - _arrow_tip + 2.0 * _tail_offset, 0.0)

    back_pt = back_pt.rotate(rotation)
    front_pt = front_pt.rotate(rotation)
    tip_1 = tip_1.rotate(rotation)
    tip_2 = tip_2.rotate(rotation)
    tail_1 = tail_1.rotate(rotation)
    tail_2 = tail_2.rotate(rotation)
    tail_3 = tail_3.rotate(rotation)
    tail_4 = tail_4.rotate(rotation)
    tail_5 = tail_5.rotate(rotation)
    tail_6 = tail_6.rotate(rotation)
    tail_start_1 = tail_start_1.rotate(rotation)
    tail_start_2 = tail_start_2.rotate(rotation)
    tail_start_3 = tail_start_3.rotate(rotation)

    arrow_draw.back_pt = back_pt
    arrow_draw.front_pt = front_pt
    arrow_draw.tip_1 = tip_1
    arrow_draw.tip_2 = tip_2
    arrow_draw.tail_1 = tail_1
    arrow_draw.tail_2 = tail_2
    arrow_draw.tail_3 = tail_3
    arrow_draw.tail_4 = tail_4
    arrow_draw.tail_5 = tail_5
    arrow_draw.tail_6 = tail_6
    arrow_draw.tail_start_1 = tail_start_1
    arrow_draw.tail_start_2 = tail_start_2
    arrow_draw.tail_start_3 = tail_start_3

    return arrow_draw


def _draw_failures(clock: Clock, drawer: PrimitiveDraw, bow_owner: BowOwner, transform: TransformComponent) -> None:
    if bow_owner.failed_to_shoot_at > 0:
        direction = _make_direction(transform.rotation)
        left_edge = direction.rotate(-_half_cone_angle)
        right_edge = direction.rotate(_half_cone_angle)
        distance = (transform.position - bow_owner.click_position).length()
        drawer.line(Color.Red, transform.position, transform.position + left_edge*distance, 2)
        drawer.line(Color.Red, transform.position, transform.position + right_edge*distance, 2)

        if (clock.total_time_s - bow_owner.failed_to_shoot_at) > _show_failure_duration:
            bow_owner.failed_to_shoot_at = 0.0


def _draw_arrow(_drawer: PrimitiveDraw, arrow_draw: ArrowDrawComponent, transform: TransformComponent) -> None:
    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.back_pt,
                 transform.position + arrow_draw.front_pt,
                 3)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.front_pt,
                 transform.position + arrow_draw.tip_1,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.front_pt,
                 transform.position + arrow_draw.tip_2,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.tail_start_1,
                 transform.position + arrow_draw.tail_1,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.tail_start_1,
                 transform.position + arrow_draw.tail_4,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.tail_start_2,
                 transform.position + arrow_draw.tail_2,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.tail_start_2,
                 transform.position + arrow_draw.tail_5,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.tail_start_3,
                 transform.position + arrow_draw.tail_3,
                 2)

    _drawer.line(Color.Yellow,
                 transform.position + arrow_draw.tail_start_3,
                 transform.position + arrow_draw.tail_6,
                 2)


def _update_arrow_movement(clock: Clock, arrow_entity: EntityHandle, storage: Storage,
                           arrow: ArrowComponent, transform: TransformComponent) -> None:
    arrow.current_speed += _deceleration * clock.delta_time_s
    arrow.distance -= arrow.current_speed * clock.delta_time_s

    if arrow.current_speed <= 5.0 or arrow.distance <= 0:
        storage.destroy_entity(arrow_entity)
        return

    transform.position += arrow.current_speed * clock.delta_time_s * arrow.direction


class _HordeMember(Query):
    handle: EntityHandle
    transform: TransformComponent
    boid_member: BoidMemberComponent


class _EnemyTotems(Query):
    handle: EntityHandle
    transform: TransformComponent
    totem: TotemComponent
    is_enemy: EnemyTotemComponent


def _detect_collisions(arrow_entity: EntityHandle, storage: Storage, env: squadron.Env,
                       arrow: ArrowComponent, arrow_transform: TransformComponent) -> None:
    arrow_position = arrow_transform.position
    squadron_comp = None

    for enemy_handle, enemy_transform, _ in storage.query(_HordeMember):
        if _collides(enemy_transform.position, arrow_position):
            storage.destroy_entity(enemy_handle)
            storage.destroy_entity(arrow_entity)

            # add soul to player!
            if squadron_comp is None:
                _squadron = env.squadron
                assert _squadron is not None
                squadron_comp = storage.get_component(_squadron, squadron.Component)
            squadron_comp.souls += 1

            return

    for totem_handle, totem_transform, totem, _ in storage.query(_EnemyTotems):
        if _collides(totem_transform.position, arrow_position):
            totem.health -= 1

            if totem.health == 0:
                storage.destroy_entity(totem_handle)


def _make_direction(rotation: float) -> pygame.Vector2:
    return pygame.Vector2(1, 0).rotate(-rotation)


def _collides(position: pygame.Vector2, other_position: pygame.Vector2) -> bool:
    return (position - other_position).length_squared() < _collision_radius_squared
