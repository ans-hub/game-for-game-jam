import pathlib

from typing import List, NamedTuple

import pygame
import pygame.mixer

import typing_json

import formats.sound_meta as format_meta


def load(path: pathlib.Path) -> 'Sound':
    meta_path = path.with_suffix(".meta")

    with open(meta_path, 'r') as f:
        disk_meta = typing_json.load(f, format_meta.Main)

    sounds = [pygame.mixer.Sound((path.parent / name).as_posix()) for name in disk_meta.files]

    meta = _parse_meta(disk_meta)
    return Sound(sounds, meta)


class Sound(NamedTuple):
    sounds: List[pygame.mixer.Sound]
    meta: 'Meta'


class Meta(NamedTuple):
    repeat: bool
    speed: float


def _parse_meta(data: format_meta.Main) -> Meta:
    return Meta(data.repeat, data.default_speed)
