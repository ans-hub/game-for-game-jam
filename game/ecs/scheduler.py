from types import CodeType
from typing import Callable, List, NamedTuple, Set, Tuple

from game.ecs.entity_handle import EntityHandle
from game.ecs.storage import Storage, is_component_type, is_env_type
from game.ecs.storage import make_handle_fetch_code, make_query_for_code, make_component_fetch_code, make_env_fetch_code


class _System(NamedTuple):
    original: Callable
    call_code: CodeType


# Should be TypeVarTuple, but it's not supported yet
class Scheduler:
    def __init__(self, *local_env_types: type) -> None:
        self._local_env_types = local_env_types
        self._systems: List[_System] = []

    def add(self, system: Callable) -> None:
        per_entity, component_types, arg_fetch_code = _parse_signature(system, self._local_env_types)

        query_iterator: str = ""

        if per_entity:
            query_iterator = make_query_for_code(component_types)

            call_source = r"""
{query_iterator}:
    system({arg_code})
            """
        else:
            call_source = r"""
system({arg_code})
            """

        call_source = call_source.format(arg_code=arg_fetch_code, query_iterator=query_iterator)

        name = "_call_{}_{}".format(system.__module__.replace('.', '_'), system.__name__)
        compiled_call = compile(call_source, name, 'exec')
        self._systems.append(_System(system, compiled_call))

    def run(self, storage: Storage, local_env: tuple) -> None:
        assert len(local_env) == len(self._local_env_types), (
            "Envs of type {} are missing from .run call"
            .format(", ".join([str(x) for x in self._local_env_types[len(local_env):]]))
        )

        for env, type_ in zip(local_env, self._local_env_types):
            assert isinstance(env, type_), "env {} is not of type {}".format(env, type_)

        args = {
            "system": Callable,
            "storage": storage,
            "local_env": local_env,
            "EntityHandle": EntityHandle
        }
        for system in self._systems:
            args['system'] = system.original
            exec(system.call_code, {}, args)


def _parse_signature(system: Callable, local_env: Tuple[type, ...]) -> Tuple[bool, Tuple[type, ...], str]:
    types = tuple(type_ for type_ in system.__annotations__.values() if type_ is not None)

    if len(types) == 0:
        return False, tuple(), ""

    component_types: List[type] = []
    arg_fetch_parts: List[str] = []

    per_entity = False

    special_types_seen: Set[type] = set()

    for type_ in types:
        if type_ in (EntityHandle, Storage, *local_env):
            assert type_ not in special_types_seen, "Only one {} is allowed in system args".format(type_)
            special_types_seen.add(type_)

            if type_ is EntityHandle:
                arg_fetch_parts.append(make_handle_fetch_code())
                per_entity = True
            elif type_ is Storage:
                arg_fetch_parts.append("storage")
            else:
                index = local_env.index(type_)
                arg_fetch_parts.append("local_env[{}]".format(index))
        elif is_component_type(type_):
            arg_fetch_parts.append(make_component_fetch_code(type_))
            component_types.append(type_)
        else:
            assert is_env_type(type_), "{} is neither component, nor env type".format(type_)
            arg_fetch_parts.append(make_env_fetch_code(type_))

    per_entity |= len(component_types) > 0

    return per_entity, tuple(component_types), ",".join(arg_fetch_parts)
