from config.action import UpperAction
import random

import pygame

from typing import List, Optional

from recordclass import RecordClass

from config import LowerAction, Direction

from game.components import TransformComponent, VelocityComponent
from game.ecs import component, Scheduler
from game.core import Clock

from game.render.system import sprite_draw

import game.resource.model.character_sheet as Model


_start_shift = (0.0, 100.0)


@component
class Component(RecordClass):
    surfaces: List[pygame.Surface]
    meta: Model.Meta

    lower_action: LowerAction = LowerAction.Idle
    upper_action: UpperAction = UpperAction.Rest
    direction: Direction = Direction.Right

    action_meta: Optional[Model.ActionMeta] = None
    animation_time: float = random.uniform(*_start_shift)

    until_idle_starts: float = 0.0


def register_update(scheduler: Scheduler) -> None:
    scheduler.add(_set_action)
    scheduler.add(_update_animations)


# TODO: @with Visible from culling
def _set_action(animation: Component, velocity: VelocityComponent, transform: TransformComponent) -> None:
    speed = velocity.velocity.length()

    lower_action = _get_lower_action(speed)

    if lower_action != animation.lower_action:
        if lower_action is LowerAction.Idle and animation.action_meta is not None:
            _schedule_delayed_idle(animation, immediate=True)

        animation.lower_action = lower_action

    animation.direction = _get_direction(transform.rotation)


# TODO: @with Visible from culling
def _update_animations(clock: Clock, animation: Component, sprite: sprite_draw.Component) -> None:
    _ensure_correct_animation(clock, animation)
    animation.animation_time += clock.delta_time
    _update_frame(animation, sprite)


def _ensure_correct_animation(clock: Clock, animation: Component) -> None:
    if animation.lower_action is LowerAction.Idle:
        animation.until_idle_starts -= clock.delta_time_s

        if animation.until_idle_starts <= 0.0 and animation.action_meta is None:
            _reset_animation(animation)
    elif animation.action_meta is None or animation.action_meta.action != animation.lower_action:
        _reset_animation(animation)


def _update_frame(animation: Component, sprite: sprite_draw.Component) -> None:
    if animation.action_meta is None:
        return

    index_in_range = _get_frame_index(animation.action_meta, animation.animation_time, animation.meta.frame_duration)

    if index_in_range is None:
        _schedule_delayed_idle(animation, immediate=False)
    else:
        frame_start = animation.action_meta.direction_to_start[animation.direction]
        frame_index = frame_start + index_in_range
        # TODO: should be other way around
        sprite.surface = animation.surfaces[animation.action_meta.surface_index]
        sprite.area = animation.meta.rects[frame_index]


def _schedule_delayed_idle(animation: Component, immediate: bool) -> None:
    animation.lower_action = LowerAction.Idle
    animation.until_idle_starts = 0.0 if immediate else random.uniform(*_idle_delay)
    animation.action_meta = None


def _reset_animation(animation: Component) -> None:
    variants = animation.meta.action_to_variants[animation.lower_action][animation.upper_action]
    animation.action_meta = random.choice(variants)
    animation.animation_time = random.uniform(*_start_shift)


def _get_frame_index(animation: Model.ActionMeta, time: float, frame_duration: float) -> Optional[int]:
    frame_index = int(time // frame_duration)

    if animation.looped:
        return frame_index % animation.frame_count
    elif frame_index < animation.frame_count:
        return frame_index
    else:
        return None


_idle_delay = (1.5, 10.0)

# TODO: move into data
_speed_to_action = (
    (1.0, LowerAction.Walk),
    (30.0, LowerAction.Trot),
    (60.0, LowerAction.Run)
)


def _get_lower_action(speed: float) -> LowerAction:
    action = LowerAction.Idle

    for start, speed_action in _speed_to_action:
        if start < speed:
            action = speed_action
        elif start >= speed:
            break

    return action


def _get_direction(rotation: float) -> Direction:
    for direction in Direction:
        if direction.min_angle <= rotation and rotation <= direction.max_angle:
            return direction

    return Direction.Right
