import numpy as np

from game.core import Position


class Terrain:
    def __init__(self, width: int, height: int) -> None:
        self.width = width
        self.height = height
        self._grid = np.zeros((width, height), dtype=np.uint8)

    def get(self, position: Position) -> int:
        return int(self._grid[position.x, position.y])

    def set(self, position: Position, tile_index: int) -> None:
        self._grid[position.x, position.y] = np.uint8(tile_index)

    def __contains__(self, position: Position) -> bool:
        if position.x < 0 or position.x >= self.width:
            return False

        if position.y < 0 or position.y >= self.height:
            return False

        return True
