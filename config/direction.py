import enum


_angle_count = 24

_sector = 360 // _angle_count
_half_sector_angle = _sector / 2

assert _half_sector_angle * 2 == _sector
assert _sector * _angle_count == 360


def _rotate(angle):
    while angle < 0.0:
        angle += 360.0

    while angle > 360.0:
        angle -= 360.0

    return angle


class Direction(enum.Enum):
    angle_0 = Right = 0
    angle_15 = 15
    angle_30 = 30
    angle_45 = UpRight = 45
    angle_60 = 60
    angle_75 = 75
    angle_90 = Up = 90
    angle_105 = 105
    angle_120 = 120
    angle_135 = UpLeft = 135
    angle_150 = 150
    angle_165 = 165
    angle_180 = Left = 180
    angle_195 = 195
    angle_210 = 210
    angle_225 = DownLeft = 225
    angle_240 = 240
    angle_255 = 255
    angle_270 = Down = 270
    angle_285 = 285
    angle_300 = 300
    angle_315 = DownRight = 315
    angle_330 = 330
    angle_345 = 345

    def __init__(self, angle: int) -> None:
        self.angle = angle
        self.min_angle = _rotate(self.angle - _half_sector_angle)
        self.max_angle = _rotate(self.angle + _half_sector_angle)


assert len(Direction) == _angle_count
assert [Direction(angle) for angle in range(0, 360, _sector)]
