from config.widget import WidgetState, FontFamily
import pathlib

from typing import Dict, List, NamedTuple, Optional, Tuple

import pygame
import pygame.image
import pygame.transform
import pygame.display

import typing_json
import formats.ui_meta as format

from game.resource.model.common import get_spritesheet_rects


class UiWidget(NamedTuple):
    name: str
    surface: pygame.Surface
    meta: 'Meta'


class Meta(NamedTuple):
    tile_size: pygame.Rect
    tiles: List[pygame.Rect]
    font: Optional['FontMeta']
    named_tiles: Dict[WidgetState, 'NamedTileMeta']


class FontMeta(NamedTuple):
    text_offset: pygame.Vector2
    text_size: int
    font_family: FontFamily


class NamedTileMeta(NamedTuple):
    tile_num: int


def load(path: pathlib.Path) -> UiWidget:
    image_path = path.with_suffix(".png")
    meta_path = path.with_suffix(".meta")

    with open(meta_path, 'r') as f:
        disk_meta = typing_json.load(f, format.Meta)

    image = pygame.image.load(image_path).convert_alpha()
    meta, image = _parse_meta(image, disk_meta)

    return UiWidget(path.name, image, meta)


def _parse_meta(image: pygame.Surface, data: format.Meta) -> Tuple[Meta, pygame.Surface]:
    identity_size = [1920, 1080]
    window_size = pygame.display.get_window_size()
    mpx = window_size[0] / identity_size[0]
    mpy = window_size[1] / identity_size[1]

    assert data.scale is not None

    window_scale = max(mpx, mpy)
    scale = data.scale * window_scale

    new_w = int(image.get_rect().w * scale)
    new_h = int(image.get_rect().h * scale)
    image = pygame.transform.scale(image, (new_w, new_h))
    orig_image_rect = image.get_rect()

    sheet_size = pygame.Rect(0, 0, data.sheet_size[0], data.sheet_size[1])

    sheet_offset = pygame.Vector2(data.sheet_offset[0] * scale, data.sheet_offset[1] * scale)
    tile_size = pygame.Rect(0, 0, data.tile_size[0] * scale, data.tile_size[1] * scale)
    font_meta: Optional[FontMeta] = None

    if data.text:
        text_offset = pygame.Vector2(data.text.text_offset[0] * scale, data.text.text_offset[1] * scale)
        text_size = data.text.text_size
        font_family = FontFamily.get(data.text.font_family)
        font_meta = FontMeta(text_offset, text_size, font_family)

    image_rect = pygame.Rect(
        sheet_offset.x,
        sheet_offset.y,
        tile_size.w * sheet_size.w,
        tile_size.h * sheet_size.h
    )
    assert image_rect.x <= orig_image_rect.x and image_rect.y <= orig_image_rect.y
    assert image_rect.w <= orig_image_rect.w and image_rect.h <= orig_image_rect.h

    frame_rect = pygame.Rect(
        0, 0,
        tile_size.w,
        tile_size.h)
    frame_rects = get_spritesheet_rects(image_rect, frame_rect)

    default_tile_num = 0

    named_tiles: Dict[WidgetState, NamedTileMeta] = {
        WidgetState.Default: NamedTileMeta(default_tile_num)
    }

    if data.named_tiles:
        for tile_name, tile_data in data.named_tiles.items():
            tile_state = WidgetState.get(tile_name)
            tile_num = tile_data.tile_coord[0] * sheet_size.w + tile_data.tile_coord[1]
            tile_meta = NamedTileMeta(tile_num)
            named_tiles[tile_state] = tile_meta

    return Meta(tile_size, frame_rects, font_meta, named_tiles), image
