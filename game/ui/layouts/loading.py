from game.resource.model.sound import Sound
from typing import NamedTuple
from game.ui.layout import Row
from game.ui.widgets import Caption, LoadingBar
from game.resource import Request, Collection
from game.ui import ui
from game.render import Window
from game.core import Clock
from game.resource.model.ui_widget import UiWidget
from game.resource.loader import Loader
from game.ui.widgets import Image, Button
from game.ui.layout import Row
from game.resource.loader import _load
from config.widget import Anchor, VAlign, HAlign
from game.ecs import component
from recordclass.typing import RecordClass


class _Assets(Collection):
    loading_bar: UiWidget = Request("loading_bar")
    logo: UiWidget = Request("logo")


_data: _Assets


class LoadingLayout(ui.Layout):
    def __init__(self, loader: Loader) -> None:
        super().__init__(loader)

        global _data
        _data = loader.load(_Assets)

        self.type = ui.LayoutType.Loading
        self.loading_bar = LoadingBar("loading_bar", frame_time=0.2, assets=_data)
        self.logo = Image("logo", assets=_data)
        # self.loading_text = Caption("Loading", size=14)

    def update(self, clock: Clock, window: Window) -> None:
        global _data

        super().update(clock, window)

        self.loading_bar.update(clock)

        self._add_rows(
            Row(
                self.logo,
                height=window.scr_h // 2,
                valign=VAlign.Bottom, halign=HAlign.Center
            ),
            Row(
                self.loading_bar,
                height=window.scr_h // 2,
                valign=VAlign.Top, halign=HAlign.Center
            ),
            valign=VAlign.Middle, margin=40
        )
