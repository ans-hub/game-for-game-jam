import pathlib
import os
from typing import List, Tuple

import pygame as pg
import pygame.display

from game.core import Color

g_fps_font_size = 16


class Window:
    def __init__(self, asset_root: pathlib.Path, w: int, h: int, caption: str, fullscreen: bool):
        self.display_flags = pg.HWSURFACE | pg.OPENGL
        self.bg_color = Color.Black
        self.scr_w = w
        self.scr_h = h
        self.fps_font_size = g_fps_font_size

        if fullscreen:
            self.display_flags = pg.FULLSCREEN
            info = pygame.display.Info()
            self.scr_w = info.current_w
            self.scr_h = info.current_h
        else:
            self.display_flags = pg.RESIZABLE

        pos = (self.scr_w, self.scr_h)
        self.surface: pg.Surface = pg.display.set_mode(pos, self.display_flags)
        self.font = os.path.join(asset_root, "fonts", 'FreeSansBold.ttf')
        self.font_surface = pg.font.Font(self.font, self.fps_font_size)

        self._texts: List[Tuple[str, int, int]]

        pg.display.set_caption(caption)

    def clear(self):
        self.surface.fill(self.bg_color)
        self._texts = []

    def blit(self):
        for text, x, y in self._texts:
            text_overlay: pg.Surface = self.font_surface.render(text, True, Color.White)
            text_rect: pg.Rect = text_overlay.get_rect()
            text_rect = text_rect.move(x, y)
            self.surface.blit(text_overlay, text_rect)

    def draw_text(self, txt: str, x: int, y: int):
        self._texts.append((txt, x, y))
