import pygame

import numpy as np


g_max_cost = np.uint8(255)


class Cost:
    def __init__(self, width: int, height: int) -> None:
        self._grid = np.zeros((width, height), dtype=np.uint8)

    def is_passable(self, position: pygame.Vector2) -> bool:
        return self.get(position) == g_max_cost

    def get(self, position: pygame.Vector2) -> np.uint8:
        return self._grid[position.x, position.y]

    def set(self, position: pygame.Vector2, value: np.uint8) -> None:
        self._grid[position.x, position.y] = value
