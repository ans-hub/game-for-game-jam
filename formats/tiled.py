import pathlib
import xml.etree.ElementTree as ET

from typing import Dict, List, NamedTuple, Tuple, Union


class TileSetRef(NamedTuple):
    id_start: int
    path: pathlib.Path


class Map(NamedTuple):
    tileset_refs: List[TileSetRef]
    size: Tuple[int, int]
    data: List[int]
    objects: List['Object']


class TileSet(NamedTuple):
    tile_size: Tuple[int, int]
    tile_count: int
    sheet_path: pathlib.Path


class Object(NamedTuple):
    name: str
    position: Tuple[float, float]
    properties: Dict[str, Union[bool, int, str, float]]


def load_map(path: pathlib.Path) -> Map:
    doc = ET.parse(path)

    tilesets_refs: List[TileSetRef] = []

    for tileset in doc.findall('./tileset'):
        meta = TileSetRef(
            int(tileset.attrib['firstgid']),
            path.parent / tileset.attrib['source']
        )
        tilesets_refs.append(meta)

    layer = doc.find('./layer')

    assert layer is not None

    size = (int(layer.attrib['width']), int(layer.attrib['height']))

    data_element = layer.find('./data')

    assert data_element is not None
    assert data_element.text is not None

    data: List[int] = [int(x) for x in data_element.text.strip().split(',')]

    objects: List[Object] = []

    for object_element in doc.findall('./objectgroup/object'):
        name = object_element.attrib['name']
        position = (float(object_element.attrib['x']), float(object_element.attrib['y']))
        properties: Dict[str, Union[bool, int, str, float]] = {}

        for property_element in object_element.findall('.//property'):
            n = property_element.attrib['name']
            t = property_element.attrib.get('type', 'str')
            v = property_element.attrib['value']

            if t == 'str':
                properties[n] = v
            elif t == 'int':
                properties[n] = int(v)
            elif t == 'bool':
                properties[n] = v == 'true'
            elif t == 'float':
                properties[n] = float(v)

        objects.append(Object(name, position, properties))

    return Map(tilesets_refs, size, data, objects)


def load_tileset(path: pathlib.Path) -> TileSet:
    doc = ET.parse(path)

    set_element = doc.find('.')

    assert set_element is not None

    tile_size = (int(set_element.attrib['tilewidth']), int(set_element.attrib['tileheight']))
    tile_count = int(set_element.attrib['tilecount'])

    image_element = set_element.find('./image')

    assert image_element is not None

    image_path = path.parent / image_element.attrib['source']

    return TileSet(tile_size, tile_count, image_path)
