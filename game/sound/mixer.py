from typing import List, Optional
import pygame
import pygame.mixer


# TODO: this is all wrong, pygame.mixer does most of this stuff (and stuff from the sound system)
class Mixer:
    _channels: List[pygame.mixer.Channel] = []

    def __init__(self) -> None:
        assert(pygame.mixer.get_init())

        self.channels_count: int = 5

        pygame.mixer.set_num_channels(self.channels_count)

    def play(self, sound: pygame.mixer.Sound, is_music: bool = False) -> Optional[pygame.mixer.Channel]:
        channel = pygame.mixer.find_channel(is_music)

        if channel is not None:
            channel.play(sound)

            if is_music:
                channel.set_volume(0.5)
            else:
                channel.set_volume(0.3)

        return channel

    def stop(self, channel: pygame.mixer.Channel) -> None:
        channel.stop()
