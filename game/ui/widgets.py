import sys
import pathlib

import pygame
import pygame.font

from typing import Callable, Any, Dict, List, Optional
from config.widget import WidgetType

from game.core import Clock, Color


g_assets_root = pathlib.Path(sys.argv[0]).parent.absolute() / "assets"  # HACK: for pyinstaller


class GenericWidget:
    def __init__(self, type: WidgetType, asset_name: Optional[str] = None, assets: Optional[Any] = None) -> None:
        self.type = type
        self.assets = assets
        self.asset_name = asset_name
        self.active_frame = 0

    @property
    def _asset(self):
        assert self.assets is not None and self.asset_name is not None
        return getattr(self.assets, self.asset_name)

    @property
    def size(self) -> pygame.Rect:
        return self._asset.meta.tile_size

    @property
    def tiles(self) -> List[pygame.Rect]:
        return self._asset.meta.tiles

    @property
    def surface(self) -> pygame.Surface:
        return self._asset.surface


class Image(GenericWidget):
    def __init__(self, asset_name: str, assets: Any) -> None:
        super().__init__(WidgetType.Image, asset_name, assets)


class FullscreenImage(GenericWidget):
    pass


class _FontCache:
    def __init__(self):
        self._fonts: Dict[int, pygame.font.Font] = {}

    def request(self, size: int) -> pygame.font.Font:
        font = self._fonts.get(size)

        if font is None:
            font = pygame.font.Font((g_assets_root / "fonts" / "FreeSansBold.ttf").as_posix(), size)
            self._fonts[size] = font

        return font


class Caption(GenericWidget):
    font_cache = _FontCache()

    def __init__(
        self,
        color: pygame.Color,
        font_size: int,
        text: str,
        background: pygame.Color = Color.Transparent
    ) -> None:
        super().__init__(WidgetType.Caption)

        self._color = color
        self._background = background
        self._font = Caption.font_cache.request(font_size)
        self._surface: pygame.Surface

        self.set(text)

    def set(self, text: str) -> None:
        # TODO: need to figure out why tranparency not applied
        self._surface = self._font.render(text, True, self._color)

    @property
    def size(self) -> pygame.Rect:
        return self._surface.get_rect()

    @property
    def tiles(self) -> List[pygame.Rect]:
        return [self._surface.get_rect()]

    @property
    def surface(self) -> pygame.Surface:
        return self._surface


class Button(GenericWidget):
    def __init__(self, asset_name: str, callback: Callable, assets: Any) -> None:
        super().__init__(WidgetType.Button, asset_name, assets)

        self.callback = callback

    def on_click_down(self) -> None:
        self.active_frame = 1

    def on_click_up(self) -> None:
        self.active_frame = 0
        self.callback()


class LoadingBar(GenericWidget):
    def __init__(self, asset_name: str, frame_time: float, assets: Any) -> None:
        super().__init__(WidgetType.AnimatedImage, asset_name, assets)

        self.active_frame = 0
        self.frame_time = frame_time
        self.time = 0.0

    def update(self, clock: Clock) -> None:
        self.time -= clock.delta_time_s

        if self.time < 0.0:
            self.time = self.frame_time
            self.active_frame += 1
            self.active_frame %= len(self.tiles)
