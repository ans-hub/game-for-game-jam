from typing import Union

import numpy as np


_index_mask = 0x00ffffff
_serial_shift = 24
_max_serial = 0xff


class EntityHandle:
    Type = 0
    InvalidValue = 0
    Invalid: 'EntityHandle'

    def __init__(self, value: Union[int, np.uint32]) -> None:
        self.value = int(value)

    @classmethod
    def make(cls, index: int, serial: int) -> 'EntityHandle':
        assert serial <= _max_serial
        value = index | (serial << _serial_shift)
        return EntityHandle(value)

    @property
    def index(self) -> int:
        return self.value & _index_mask

    @property
    def serial(self) -> int:
        return self.value >> _serial_shift

    def __eq__(self, other: object) -> bool:
        return isinstance(other, EntityHandle) and self.value == other.value

    def __lt__(self, other: 'EntityHandle') -> bool:
        return self.value < other.value

    def __hash__(self) -> int:
        return int(self.value)


EntityHandle.Invalid = EntityHandle(EntityHandle.InvalidValue)
