from typing import NamedTuple


class Position(NamedTuple):
    x: int
    y: int

    def __add__(self, other: object) -> 'Position':
        if isinstance(other, Position):
            return Position(self.x + other.x, self.y + other.y)

        return NotImplemented

    def __mul__(self, other: object) -> 'Position':
        if isinstance(other, int):
            return Position(self.x * other, self.y * other)

        return NotImplemented
