from typing import List
import pygame as pg


class KeyHandler:
    def on_key_down(self, key: int) -> None: ...
    def on_key_up(self, key: int) -> None: ...
    def on_mouse_up(self, button: int) -> None: ...
    def on_mouse_down(self, button: int) -> None: ...


class EventPump:
    def __init__(self):
        self.is_running = True
        self.key_handlers: List[KeyHandler] = []

    def add_key_handler(self, handler: KeyHandler) -> None:
        self.key_handlers.append(handler)

    def update(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.is_running = False
            elif event.type == pg.KEYDOWN:
                _notify(self.key_handlers, 'on_key_down', event.key)
            elif event.type == pg.KEYUP:
                _notify(self.key_handlers, 'on_key_up', event.key)
            elif event.type == pg.MOUSEBUTTONUP:
                _notify(self.key_handlers, 'on_mouse_up', event.button)
            elif event.type == pg.MOUSEBUTTONDOWN:
                _notify(self.key_handlers, 'on_mouse_down', event.button)


def _notify(handlers: list, api: str, *args) -> None:
    for handler in handlers:
        call = getattr(handler, api)
        call(*args)
