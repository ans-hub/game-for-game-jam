import enum


class _Action(str, enum.Enum):
    _ignore_ = 'id looped'

    id: str
    looped: bool

    def __new__(cls, id: str, looped: bool = False):
        obj = str.__new__(cls, [id])
        obj._value_ = id
        obj.id = id
        obj.looped = looped
        return obj

    def __lt__(self, other) -> bool:
        if self.__class__ is other.__class__:
            return self.id < other.id

        return NotImplemented

    def __eq__(self, other) -> bool:
        if self.__class__ is other.__class__:
            return self.id == other.id

        return NotImplemented

    def __hash__(self) -> int:
        return self.id.__hash__()


class LowerAction(_Action):
    Idle = ('idle', False)

    Walk = ('walk', True)
    Trot = ('trot', True)
    Run = ('run', True)

    Back = ('back', True)


class UpperAction(_Action):
    Rest = ('rest', True)
    Fire = ('fire', False)
