import pathlib
import sys

# HACK: pass args to blender and move the sprite merging and selection logic out of the blender
self = pathlib.Path(__file__).parent
root = self.parent.parent

sys.path += [root.as_posix(), self.as_posix()]

from common import export  # noqa
from common import LowerActionSelector, Pose, UpperActionSelector, AnimationSet, LowerAction, UpperAction, Config  # noqa


export(Config(
    layer='archer',
    frames_per_sprite=5,
    render_resolution=(256, 256),
    sprite_size=128,
    animation_tree={
        'Horse_Armature': LowerActionSelector({
            LowerAction.Walk: AnimationSet('Horse_walk'),
            LowerAction.Trot: AnimationSet('Horse_Trot'),
            LowerAction.Run: AnimationSet('Horse_Gallop'),
            LowerAction.Idle: AnimationSet(
                'Horse_idle_A',
                'Horse_idle_B',
                'Horse_idle_C',
                'Horse_idle_D',
                'Horse_idle_Feeding'
            ),
            LowerAction.Back: AnimationSet('Horse_back')
        }),
        'Biped': UpperActionSelector({
            UpperAction.Fire: Pose('R_Attack_Archer_Hip', 'Fire'),
            UpperAction.Rest: LowerActionSelector({
                LowerAction.Walk: AnimationSet('R_Walk_Ready_Archer'),
                LowerAction.Trot: AnimationSet('R_Trot_Archer_Rdy'),
                LowerAction.Run: AnimationSet('R_Gallop_Archer'),
                LowerAction.Idle: AnimationSet('R_Idle_Archer_Ready'),
                LowerAction.Back: AnimationSet('R_Walk_Ready_Archer')
            })
        }),
        'Bow_Armature': UpperActionSelector({
            UpperAction.Fire: Pose('Bow_Fire.001', 'Fire'),
            UpperAction.Rest: Pose('Bow_Fire.001', 'Ready')
        })
    }
))
