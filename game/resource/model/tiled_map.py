from game.core.position import Position
import pathlib

from typing import Dict, List, NamedTuple, Union

import pygame
import pygame.image

import formats.tiled as format

from game import grid
from game.resource.model.common import get_spritesheet_rects


class TiledMap(NamedTuple):
    grid: grid.Terrain
    tiles: List['TerrainTile']
    objects: List['Object']


class TerrainTile(NamedTuple):
    surface: pygame.Surface
    rect: pygame.Rect


class Object(NamedTuple):
    name: str
    position: pygame.Vector2
    properties: Dict[str, Union[bool, int, str, float]] = {}


class _Tileset(NamedTuple):
    id_start: int
    tiles: List[TerrainTile]


def load(path: pathlib.Path) -> 'TiledMap':
    info = format.load_map(path.with_suffix('.tmx'))

    tilesets = [_load_tileset(ref) for ref in info.tileset_refs]
    tilesets.sort(key=lambda x: x.id_start)

    tiles = [tile for tileset in tilesets for tile in tileset.tiles]

    # ids starts with 1
    assert len(tiles) == (tilesets[-1].id_start + len(tilesets[-1].tiles) - 1)

    terrain = grid.Terrain(info.size[0], info.size[1])

    for i, tile_id in enumerate(info.data):
        x = i % terrain.width
        y = i // terrain.width

        terrain.set(Position(x, y), tile_id - 1)

    objects = []

    for object in info.objects:
        objects.append(Object(object.name, pygame.Vector2(*object.position), object.properties))

    return TiledMap(terrain, tiles, objects)


def _load_tileset(ref: format.TileSetRef) -> _Tileset:
    info = format.load_tileset(ref.path)

    surface = pygame.image.load(info.sheet_path).convert_alpha()
    rects = get_spritesheet_rects(surface.get_rect(), pygame.Rect(0, 0, *info.tile_size))

    rects = rects[:info.tile_count]

    tiles = [TerrainTile(surface, rect) for rect in rects]

    return _Tileset(ref.id_start, tiles)
