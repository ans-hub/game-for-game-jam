from game.components import TotemComponent
from game.core import Clock, Color

from game.ecs import Storage, Query

from game.render import Window

from game.resource import Request, Collection
from game.resource.model.ui_widget import UiWidget
from game.resource.loader import Loader

from game.logic.system import squadron

from game.ui import ui
from game.ui.widgets import Image, Button, Caption
from game.ui.layout import Row, LayoutType

from config.widget import VAlign, HAlign


class _Assets(Collection):
    icon_soul: UiWidget = Request("icon_soul")
    icon_warrior: UiWidget = Request("icon_warrior")
    icon_totem: UiWidget = Request("icon_totem")
    btn_menu: UiWidget = Request("btn_menu")


_data: _Assets


class _TotemQuery(Query):
    totem: TotemComponent


class HudLayout(ui.Layout):
    def __init__(self, loader: Loader, storage: Storage) -> None:
        super().__init__(loader)

        global _data
        _data = loader.request(_Assets)

        self.type = ui.LayoutType.Hud
        self.btn_menu: Button = Button("btn_menu", lambda: self._change_layout(LayoutType.Hud, LayoutType.Menu), _data)

        self.icon_soul: Image = Image("icon_soul", assets=_data)
        self.label_soul_counter: Caption = Caption(Color.White, 45, "0")

        self.icon_rider: Image = Image("icon_warrior", assets=_data)
        self.label_rider_counter: Caption = Caption(Color.White, 45, "0")

        self.icon_totem: Image = Image("icon_totem", assets=_data)
        self.label_totem_counter: Caption = Caption(Color.White, 45, "0")

        self.storage = storage

    def update(self, clock: Clock, window: Window) -> None:
        global _data

        super().update(clock, window)

        env = self.storage.get_env(squadron.Env)
        assert env.squadron is not None
        squadron_ = self.storage.get_component(env.squadron, squadron.Component)

        totem_soul_count = 0
        enemy_totem_count = 0

        for (totem_, ) in self.storage.query(_TotemQuery):
            if totem_.is_enemy:
                enemy_totem_count += 1
            else:
                totem_soul_count += totem_.souls

        self.label_soul_counter.set("{:02}/{:02}".format(squadron_.souls, totem_soul_count))
        self.label_rider_counter.set("{:02}".format(squadron_.rider_count))
        self.label_totem_counter.set("{:02}".format(enemy_totem_count))

        self._add_rows(
            Row(
                self.icon_rider,
                self.label_rider_counter,
                self.icon_soul,
                self.label_soul_counter,
                self.icon_totem,
                self.label_totem_counter,
                height=60,
                valign=VAlign.Middle, halign=HAlign.Center
            ),
            valign=VAlign.Top, margin=20
        )

        self._add_rows(
            Row(
                self.btn_menu,
                height=40,
                valign=VAlign.Top, halign=HAlign.Right
            ),
            valign=VAlign.Top, margin=20
        )
