<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.1" name="buildings" tilewidth="1095" tileheight="1098" tilecount="117" columns="0">
 <grid orientation="isometric" width="1" height="1"/>
 <tile id="0">
  <image width="653" height="824" source="building_001.png"/>
 </tile>
 <tile id="1">
  <image width="1093" height="934" source="building_002.png"/>
 </tile>
 <tile id="2">
  <image width="1093" height="1090" source="building_003.png"/>
 </tile>
 <tile id="3">
  <image width="1095" height="1098" source="building_004.png"/>
 </tile>
 <tile id="4">
  <image width="653" height="824" source="building_005.png"/>
 </tile>
 <tile id="5">
  <image width="1093" height="934" source="building_006.png"/>
 </tile>
 <tile id="6">
  <image width="1093" height="1090" source="building_007.png"/>
 </tile>
 <tile id="7">
  <image width="1095" height="1098" source="building_008.png"/>
 </tile>
 <tile id="8">
  <image width="653" height="824" source="building_009.png"/>
 </tile>
 <tile id="9">
  <image width="1093" height="934" source="building_010.png"/>
 </tile>
 <tile id="10">
  <image width="1093" height="1090" source="building_011.png"/>
 </tile>
 <tile id="11">
  <image width="1095" height="1098" source="building_012.png"/>
 </tile>
 <tile id="12">
  <image width="536" height="414" source="building_013.png"/>
 </tile>
 <tile id="13">
  <image width="537" height="414" source="building_014.png"/>
 </tile>
 <tile id="14">
  <image width="536" height="414" source="building_015.png"/>
 </tile>
 <tile id="15">
  <image width="746" height="636" source="building_016.png"/>
 </tile>
 <tile id="16">
  <image width="746" height="636" source="building_017.png"/>
 </tile>
 <tile id="17">
  <image width="746" height="636" source="building_018.png"/>
 </tile>
 <tile id="18">
  <image width="763" height="662" source="building_019.png"/>
 </tile>
 <tile id="19">
  <image width="763" height="662" source="building_020.png"/>
 </tile>
 <tile id="20">
  <image width="763" height="662" source="building_021.png"/>
 </tile>
 <tile id="21">
  <image width="763" height="662" source="building_022.png"/>
 </tile>
 <tile id="22">
  <image width="763" height="662" source="building_023.png"/>
 </tile>
 <tile id="23">
  <image width="364" height="455" source="building_024.png"/>
 </tile>
 <tile id="24">
  <image width="364" height="455" source="building_025.png"/>
 </tile>
 <tile id="25">
  <image width="615" height="393" source="building_026.png"/>
 </tile>
 <tile id="26">
  <image width="689" height="372" source="building_027.png"/>
 </tile>
 <tile id="27">
  <image width="253" height="187" source="building_028.png"/>
 </tile>
 <tile id="28">
  <image width="263" height="286" source="building_029.png"/>
 </tile>
 <tile id="29">
  <image width="907" height="567" source="building_030.png"/>
 </tile>
 <tile id="30">
  <image width="643" height="444" source="building_031.png"/>
 </tile>
 <tile id="31">
  <image width="259" height="282" source="building_032.png"/>
 </tile>
 <tile id="32">
  <image width="895" height="560" source="building_033.png"/>
 </tile>
 <tile id="33">
  <image width="635" height="438" source="building_034.png"/>
 </tile>
 <tile id="34">
  <image width="259" height="282" source="building_035.png"/>
 </tile>
 <tile id="35">
  <image width="895" height="560" source="building_036.png"/>
 </tile>
 <tile id="36">
  <image width="635" height="438" source="building_037.png"/>
 </tile>
 <tile id="37">
  <image width="259" height="282" source="building_038.png"/>
 </tile>
 <tile id="38">
  <image width="895" height="560" source="building_039.png"/>
 </tile>
 <tile id="39">
  <image width="635" height="438" source="building_040.png"/>
 </tile>
 <tile id="40">
  <image width="259" height="282" source="building_041.png"/>
 </tile>
 <tile id="41">
  <image width="895" height="560" source="building_042.png"/>
 </tile>
 <tile id="42">
  <image width="635" height="438" source="building_043.png"/>
 </tile>
 <tile id="43">
  <image width="508" height="334" source="building_044.png"/>
 </tile>
 <tile id="44">
  <image width="537" height="335" source="building_045.png"/>
 </tile>
 <tile id="45">
  <image width="505" height="334" source="building_046.png"/>
 </tile>
 <tile id="46">
  <image width="507" height="335" source="building_047.png"/>
 </tile>
 <tile id="47">
  <image width="508" height="335" source="building_048.png"/>
 </tile>
 <tile id="48">
  <image width="508" height="334" source="building_049.png"/>
 </tile>
 <tile id="49">
  <image width="507" height="335" source="building_050.png"/>
 </tile>
 <tile id="50">
  <image width="508" height="334" source="building_051.png"/>
 </tile>
 <tile id="51">
  <image width="507" height="335" source="building_052.png"/>
 </tile>
 <tile id="52">
  <image width="508" height="335" source="building_053.png"/>
 </tile>
 <tile id="53">
  <image width="507" height="334" source="building_054.png"/>
 </tile>
 <tile id="54">
  <image width="507" height="334" source="building_055.png"/>
 </tile>
 <tile id="55">
  <image width="952" height="783" source="building_056.png"/>
 </tile>
 <tile id="56">
  <image width="952" height="783" source="building_057.png"/>
 </tile>
 <tile id="57">
  <image width="645" height="460" source="building_058.png"/>
 </tile>
 <tile id="58">
  <image width="645" height="460" source="building_059.png"/>
 </tile>
 <tile id="59">
  <image width="645" height="460" source="building_060.png"/>
 </tile>
 <tile id="60">
  <image width="665" height="527" source="building_061.png"/>
 </tile>
 <tile id="61">
  <image width="665" height="527" source="building_062.png"/>
 </tile>
 <tile id="62">
  <image width="665" height="527" source="building_063.png"/>
 </tile>
 <tile id="63">
  <image width="665" height="527" source="building_064.png"/>
 </tile>
 <tile id="64">
  <image width="665" height="527" source="building_065.png"/>
 </tile>
 <tile id="65">
  <image width="665" height="527" source="building_066.png"/>
 </tile>
 <tile id="66">
  <image width="603" height="479" source="building_067.png"/>
 </tile>
 <tile id="67">
  <image width="603" height="479" source="building_068.png"/>
 </tile>
 <tile id="68">
  <image width="603" height="479" source="building_069.png"/>
 </tile>
 <tile id="69">
  <image width="603" height="479" source="building_070.png"/>
 </tile>
 <tile id="70">
  <image width="603" height="479" source="building_071.png"/>
 </tile>
 <tile id="71">
  <image width="603" height="479" source="building_072.png"/>
 </tile>
 <tile id="72">
  <image width="595" height="468" source="building_073.png"/>
 </tile>
 <tile id="73">
  <image width="531" height="486" source="building_074.png"/>
 </tile>
 <tile id="74">
  <image width="574" height="424" source="building_075.png"/>
 </tile>
 <tile id="75">
  <image width="528" height="395" source="building_076.png"/>
 </tile>
 <tile id="76">
  <image width="225" height="140" source="building_077.png"/>
 </tile>
 <tile id="77">
  <image width="774" height="711" source="building_078.png"/>
 </tile>
 <tile id="78">
  <image width="780" height="702" source="building_079.png"/>
 </tile>
 <tile id="79">
  <image width="499" height="472" source="building_080.png"/>
 </tile>
 <tile id="80">
  <image width="699" height="586" source="building_081.png"/>
 </tile>
 <tile id="81">
  <image width="699" height="586" source="building_082.png"/>
 </tile>
 <tile id="82">
  <image width="699" height="559" source="building_083.png"/>
 </tile>
 <tile id="83">
  <image width="444" height="470" source="building_084.png"/>
 </tile>
 <tile id="84">
  <image width="464" height="445" source="building_085.png"/>
 </tile>
 <tile id="85">
  <image width="384" height="291" source="building_086.png"/>
 </tile>
 <tile id="86">
  <image width="475" height="409" source="building_087.png"/>
 </tile>
 <tile id="87">
  <image width="403" height="303" source="building_088.png"/>
 </tile>
 <tile id="88">
  <image width="317" height="332" source="building_089.png"/>
 </tile>
 <tile id="89">
  <image width="506" height="498" source="building_090.png"/>
 </tile>
 <tile id="90">
  <image width="964" height="370" source="building_091.png"/>
 </tile>
 <tile id="91">
  <image width="375" height="349" source="building_092.png"/>
 </tile>
 <tile id="92">
  <image width="779" height="485" source="building_093.png"/>
 </tile>
 <tile id="93">
  <image width="769" height="658" source="building_094.png"/>
 </tile>
 <tile id="94">
  <image width="655" height="552" source="building_095.png"/>
 </tile>
 <tile id="95">
  <image width="812" height="629" source="building_096.png"/>
 </tile>
 <tile id="96">
  <image width="500" height="618" source="building_097.png"/>
 </tile>
 <tile id="97">
  <image width="500" height="618" source="building_098.png"/>
 </tile>
 <tile id="98">
  <image width="789" height="769" source="building_099.png"/>
 </tile>
 <tile id="99">
  <image width="757" height="582" source="building_100.png"/>
 </tile>
 <tile id="100">
  <image width="602" height="656" source="building_101.png"/>
 </tile>
 <tile id="101">
  <image width="580" height="411" source="building_102.png"/>
 </tile>
 <tile id="102">
  <image width="774" height="716" source="building_103.png"/>
 </tile>
 <tile id="103">
  <image width="616" height="596" source="building_104.png"/>
 </tile>
 <tile id="104">
  <image width="332" height="304" source="building_105.png"/>
 </tile>
 <tile id="105">
  <image width="332" height="304" source="building_106.png"/>
 </tile>
 <tile id="106">
  <image width="332" height="304" source="building_107.png"/>
 </tile>
 <tile id="107">
  <image width="568" height="582" source="building_108.png"/>
 </tile>
 <tile id="108">
  <image width="568" height="582" source="building_109.png"/>
 </tile>
 <tile id="109">
  <image width="568" height="582" source="building_110.png"/>
 </tile>
 <tile id="110">
  <image width="568" height="582" source="building_111.png"/>
 </tile>
 <tile id="111">
  <image width="568" height="582" source="building_112.png"/>
 </tile>
 <tile id="112">
  <image width="568" height="582" source="building_113.png"/>
 </tile>
 <tile id="113">
  <image width="559" height="488" source="building_114.png"/>
 </tile>
 <tile id="114">
  <image width="803" height="642" source="building_115.png"/>
 </tile>
 <tile id="115">
  <image width="527" height="545" source="building_116.png"/>
 </tile>
 <tile id="116">
  <image width="602" height="421" source="building_117.png"/>
 </tile>
</tileset>
