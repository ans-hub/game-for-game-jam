from config import Direction
from typing import Dict, List, Optional, Set, Tuple

import enum
import pygame
import pygame.mouse

from game.core.event_pump import KeyHandler


class MouseEvents(enum.IntEnum):
    LMB = 0,
    MMB = 1,
    RMB = 2,
    WU = 3,
    WD = 4


class Input(KeyHandler):
    def __init__(self):
        self.direction: Optional[Direction] = None
        self.rider_count = 0
        self.debug_draw = False

        self.mb_down_pos: List[pygame.Vector2] = [pygame.Vector2(0, 0)] * len(MouseEvents)
        self.mb_up: List[bool] = [False] * len(MouseEvents)
        self.mb_up_pos: List[pygame.Vector2] = [pygame.Vector2(0, 0)] * len(MouseEvents)
        self.mb_down_times: List[int] = [0] * len(MouseEvents)
        self.mouse_position = pygame.Vector2(0, 0)

        self._direction_states: Set[Direction] = set()
        self._permanent_direction: bool = False

    def update(self) -> None:
        self.mouse_position = pygame.mouse.get_pos()

        for i in range(len(MouseEvents)):
            if self.mb_down_times[i] != 0:
                self.mb_down_times[i] += 1
            if self.mb_up[i]:
                self.mb_down_times[i] = 0
                self.mb_up[i] = False

    def on_key_down(self, key: int) -> None:
        direction = _key_to_direction.get(key)

        if direction is not None:
            self._direction_states.add(direction)
            self._set_direction()

    def on_key_up(self, key: int) -> None:
        if key == pygame.K_PAGEUP:
            self.rider_count += 1
        elif key == pygame.K_PAGEDOWN:
            self.rider_count -= 1
        elif key == pygame.K_F3:
            self.debug_draw = not self.debug_draw
        elif key == pygame.K_F4:
            self._permanent_direction = not self._permanent_direction
        else:
            direction = _key_to_direction.get(key)

            if direction is not None:
                try:
                    self._direction_states.remove(direction)
                except KeyError:
                    pass  # Happens if key was pressed before loop started and released after
                self._set_direction()

    def _set_direction(self):
        if len(self._direction_states) == 1:
            self.direction = next(iter(self._direction_states))
        elif len(self._direction_states) == 2:
            for key, result in _direction_pairs_to_direction:
                if key[0] in self._direction_states and key[1] in self._direction_states:
                    self.direction = result
                    break
            else:
                self.direction = None
        elif not self._permanent_direction:
            self.direction = None

    def on_mouse_up(self, key: int) -> None:
        try:
            self.mb_down_times[key-1] = 0
            self.mb_up[key-1] = True
            self.mb_up_pos[key-1] = self.mouse_position
        except IndexError:
            pass

    def on_mouse_down(self, key: int) -> None:
        try:
            self.mb_down_times[key-1] = 1
            self.mb_down_pos[key-1] = self.mouse_position
        except IndexError:
            pass

    def is_lmb_pressed(self) -> bool:
        return self.mb_down_times[MouseEvents.LMB] > 0

    def is_lmb_clicked(self) -> bool:
        return self.mb_down_times[MouseEvents.LMB] == 1

    def is_lmb_up(self) -> bool:
        return self.mb_up[MouseEvents.LMB]


_direction_pairs_to_direction: List[Tuple[Tuple[Direction, Direction], Direction]] = [
    ((Direction.Up, Direction.Left), Direction.UpLeft),
    ((Direction.Up, Direction.Right), Direction.UpRight),
    ((Direction.Down, Direction.Left), Direction.DownLeft),
    ((Direction.Down, Direction.Right), Direction.DownRight)
]


_key_to_direction: Dict[int, Direction] = {
    pygame.K_w: Direction.Up,
    pygame.K_s: Direction.Down,
    pygame.K_a: Direction.Left,
    pygame.K_d: Direction.Right
}
