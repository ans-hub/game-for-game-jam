from typing import List
import pygame


def get_spritesheet_rects(image_size: pygame.Rect, tile_rect: pygame.Rect) -> List[pygame.Rect]:
    assert image_size.w % tile_rect.w == 0, "Spritesheet widght is not a multiplication of {}".format(tile_rect.w)
    assert image_size.h % tile_rect.h == 0, "Spritesheet height is not a multiplication of {}".format(tile_rect.h)

    rects: List[pygame.Rect] = []

    count_w: int = image_size.w // tile_rect.w
    count_h: int = image_size.h // tile_rect.h

    for y_index in range(count_h):
        for x_index in range(count_w):
            x_shift = x_index * tile_rect.w
            y_shift = y_index * tile_rect.h
            rect = tile_rect.move(x_shift, y_shift)
            rects.append(rect)

    return rects
