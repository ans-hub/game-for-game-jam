import pygame


class Camera:
    def __init__(self, window_size: pygame.Rect) -> None:
        self.half_size = pygame.Vector2(window_size.w // 2, window_size.h // 2)
        self.wpos = pygame.Vector2(0, 0)
        self.top_left = pygame.Vector2(0, 0)
        self.bottom_right = pygame.Vector2(0, 0)

    @property
    def top(self) -> float:
        return self.top_left.y

    @property
    def left(self) -> float:
        return self.top_left.x

    @property
    def bottom(self) -> float:
        return self.bottom_right.y

    @property
    def right(self) -> float:
        return self.bottom_right.x

    def update(self) -> None:
        self.top_left = self.wpos - self.half_size
        self.bottom_right = self.wpos + self.half_size

    def move(self, move: pygame.Vector2) -> None:
        self.wpos += move

    def is_in_bounds(self, wpos: pygame.Vector2) -> bool:
        hor_in_bound = wpos.x >= self.left and wpos.x < self.right
        vert_in_bound = wpos.y >= self.top and wpos.y < self.bottom

        return hor_in_bound and vert_in_bound
