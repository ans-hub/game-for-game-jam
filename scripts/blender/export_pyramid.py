import pathlib
import sys

# HACK: pass args to blender and move the sprite merging and selection logic out of the blender
self = pathlib.Path(__file__).parent
root = self.parent.parent

sys.path += [root.as_posix(), self.as_posix()]

from common import export  # noqa
from common import LowerActionSelector, AnimationSet, LowerAction, UpperAction, Config  # noqa


def _export(layer: str, quit: bool) -> None:
    export(Config(
        layer=layer,
        frames_per_sprite=5,
        render_resolution=(512, 512),
        sprite_size=256,
        animation_tree={
            'Root': LowerActionSelector({
                LowerAction.Idle: AnimationSet('Float')
            }),
            'Gem_Red': LowerActionSelector({
                LowerAction.Idle: AnimationSet('Float_Red')
            }),
            'Gem_Light_Red': LowerActionSelector({
                LowerAction.Idle: AnimationSet('Float_Red_Light')
            }),
            'Gem_Green': LowerActionSelector({
                LowerAction.Idle: AnimationSet('Float_Green')
            }),
            'Gem_Light_Green': LowerActionSelector({
                LowerAction.Idle: AnimationSet('Float_Green_Light')
            }),
        },
        lower_action_aliases={
            LowerAction.Walk: LowerAction.Idle,
            LowerAction.Trot: LowerAction.Idle,
            LowerAction.Run: LowerAction.Idle,
            LowerAction.Back: LowerAction.Idle
        },
        lower_actions=(LowerAction.Idle,),
        upper_actions=(UpperAction.Rest,),
        looped_actions={
            LowerAction.Idle: True
        }
    ), quit=quit)


_export('totem_player', False)
_export('totem_enemy', True)
