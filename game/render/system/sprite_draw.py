import pygame

from recordclass import RecordClass

from game.components import TransformComponent

from game.render.window import Window
from game.render.camera import Camera

from game.ecs import component, Scheduler, Storage, Query
from game.render.system import culling


@component
class Component(RecordClass):
    surface: pygame.Surface
    area: pygame.Rect


def register_draw(scheduler: Scheduler) -> None:
    scheduler.add(_draw)


class _DrawableEntity(Query):
    sprite: Component
    culling: culling.Component
    transform: TransformComponent


def _draw(storage: Storage, window: Window, camera: Camera) -> None:
    to_cam_space = camera.wpos * -1.0
    to_scr_space: pygame.Vector2 = to_cam_space + camera.half_size

    sorted_surfaces = sorted((
        (
            sprite.surface,
            transform.position + to_scr_space - pygame.Vector2(sprite.area.h // 2, sprite.area.w // 2),
            sprite.area,
            transform.z_level
        )
        for sprite, culling, transform in storage.query(_DrawableEntity)
        if culling.visible
    ), key=lambda x: (x[3], x[1].y, x[1].x))

    # HACK: pygame interface stubs have no overloads for blits
    surfaces = [x[:-1] for x in sorted_surfaces]

    window.surface.blits(surfaces, doreturn=False)  # type: ignore
